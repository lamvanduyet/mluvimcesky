<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FormsTest extends TestCase
{
    // /**
    //  * A basic test example.
    //  *
    //  * @return void
    //  */
    // public function testExample()
    // {
    //     $this->assertTrue(true);
    // }

    // /**
    //  * testing login forms
    //  *
    //  * @return void
    //  */
    // public function testNewUserRegisterForm()
    // {
    //      $this->visit('/register')
    //      ->type('tester', 'name')
    //      ->type('tester', 'username')
    //      ->type('tester@email.com', 'email')
    //      ->type('password', 'password')
    //      ->type('password', 'password_confirmation')
    //      ->press('Přidej se')
    //      ->seePageIs('/');

    // }

    /**
     * testing login form
     *
     * @return void
     */
    public function testUserLoginForm()
    {
         $this->visit('/login')
         ->type('tester@email.com', 'email')
         ->type('password', 'password')
         ->check('remember')
         ->press('Přihlášení')
         ->seePageIs('/home');
    }

    /** testing newsletter form
     *
     * @return void
     */
    public function testNewsletterForm()
    {
         $this->visit('/')
         ->type('tester@email.com', 'news_email')
         ->press('Odebírat')
         ->seePageIs('/');
    }
}
