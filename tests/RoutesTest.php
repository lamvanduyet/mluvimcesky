<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoutesTest extends TestCase
{
    // /**
    //  * A basic test example.
    //  *
    //  * @return void
    //  */
    // public function testExample()
    // {
    //     $this->assertTrue(true);
    // }
    
    /**
     * testing lessons routes
     *
     * @return void
     */
    public function testLessonsRoutes()
    {
         $this->visit('/')
         ->click('Lekce')
         ->seePageIs('/lessons');
    }

    /**
     * testing tests routes
     *
     * @return void
     */
    public function testTestsRoutes()
    {
         $this->visit('/')
         ->click('Testy')
         ->seePageIs('/tests');
    }

    /**
     * testing pricing routes
     *
     * @return void
     */
    public function testPricingRoutes()
    {
         $this->visit('/')
         ->click('Ceník')
         ->seePageIs('/pricing');
    }

    /**
     * testing contact routes
     *
     * @return void
     */
    public function testContactRoutes()
    {
         $this->visit('/')
         ->click('Kontakt')
         ->seePageIs('/contact');
    }
}
