<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    /**
     * relationship with Answers model
     * @return hasMany relationship
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
	 * relationship with User model
	 * @return belongsTo relationship
	 */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * relationship with Test model
     * @return belongsTo relationship
     */
    public function test()
    {
        return $this->belongsTo('App\Test');
    }
}
