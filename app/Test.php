<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use Searchable;
    
    /**
     * global query scope
     * @param  $query
     * @param  $slug
     * @return query scope
     */
    public function scopeSlug($query, $slug)
    {
        $query->where('slug', $slug);
    }

    /**
	 * relationship with User model
	 * @return belongsTo relationship
	 */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * relationship with Chapter model
     * @return belongsTo relationship
     */
    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }

    /**
     * relationship with Question model
     * @return hasMany relationship
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * relationship with Submission model
     * @return hasMany relationship
     */
    public function submissions()
    {
        return $this->hasMany('App\Submission');
    }
}
