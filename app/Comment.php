<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
	 * relationship with User model
	 * @return belongsTo relationship
	 */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
