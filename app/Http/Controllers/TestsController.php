<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestsController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
    }
    
	/**
	 * Show all tests
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $tests = Test::latest()->paginate(9);
    	return view('pages.tests', compact('tests'));
    }

    /**
	 * Show specific test
	 * @return \Illuminate\Http\Response
	 */
    public function test($slug){
        $test = Test::slug($slug)->firstOrFail();

        /*
         * If the test is 'paid' user must be logged in (have paid for register)
         */
        if($test->is_paid){
            if(Auth::check()){
                if(auth()->user()->subscribed('main')){
                    return view('pages.test', compact('test'));
                }else{
                    return redirect('/login');    
                }
            }else {
                return redirect('/login');   
            }
        }else{
            return view('pages.test', compact('test'));
        }
    	
    }

}
