<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Submission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubmissionsController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
        $this->middleware('auth');
    }

    public function index(){
    	$user = User::findOrFail(Auth::id());
    	$submissions = Submission::where('user_id', $user->id)->latest()->paginate(10);
    	return view('pages.submissions', compact('submissions'));
    }
}
