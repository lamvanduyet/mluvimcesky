<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LessonsController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
    }
    
	/**
	 * Show all lessons
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $lessons = Lesson::latest()->paginate(9);
    	return view('pages.lessons', compact('lessons'));
    }

    /**
	 * Show specific lesson
	 * @return \Illuminate\Http\Response
	 */
    public function lesson($slug){

        $lesson = Lesson::slug($slug)->firstOrFail();

        /*
         * If the lesson is paid user must be logged in (have paid for register)
         */
        if($lesson->is_paid){
            if(Auth::check()){
                if(auth()->user()->subscribed('main')){
                    return view('pages.lesson', compact('lesson'));
                }else{
                    return redirect('/login');    
                }
            }else {
                return redirect('/login');   
            }
        }else{
            return view('pages.lesson', compact('lesson'));
        }
    	
    }
}
