<?php

namespace App\Http\Controllers;

use Alert;
use App\Chapter;
use App\Choice;
use App\Http\Requests;
use App\Lesson;
use App\Question;
use App\Test;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
        $this->middleware('admin');
    }

	/**
	 * show login for admin
	 * @return route to dashboard
	 */
    public function index() 
    {
    	return redirect('/admin/dashboard');
    }

	/**
	 * show dashboard
	 * @return \Illuminate\Http\Response
	 */
    public function dashboard() 
    {
        $lessons = Lesson::all();
        $tests = Test::all();
    	return view('admin.dashboard', compact('lessons', 'tests'));
    }

    /**
	 * show users table for admin
	 * @return \Illuminate\Http\Response
	 */
    public function users() 
    {
        $users = User::all();
    	return view('admin.users', compact('users'));
    }


    /**
     * show chapters table for admin
     * @return \Illuminate\Http\Response
     */
    public function chapters() 
    {
        $chapters = Chapter::all();
        return view('admin.chapters', compact('chapters'));
    }

    /**
     * show view for adding a chapter
     * @return \Illuminate\Http\Response
     */
    public function addChapter() 
    {
        return view('admin.add-chapter');
    }

    /**
     * handle form request for creating chapter
     * @return \Illuminate\Http\Response
     */
    public function chapterCreate(Request $request) 
    {
        $this->validate($request, ['title' => 'required|max:255', 'slug' => 'required|max:255|unique:lessons', 'description' => 'required|min:6', 'difficulty' => 'required', 'is_paid' => 'required', 'estimated_time' => 'required']);

        //creating new chapter
        $chapter = new Chapter();
        $chapter->title = $request->title;
        $chapter->slug = $request->slug;
        $chapter->description = $request->description;
        $chapter->difficulty = $request->difficulty;
        $chapter->is_paid = $request->is_paid;
        $chapter->estimated_time = $request->estimated_time;
        
        Auth::user()->chapters()->save($chapter);

        alert()->success('You have successfully created the chapter!', 'Created')->autoclose(2000);
        return back();
    }

    /**
     * show view for editing the chapter
     * @return \Illuminate\Http\Response
     */
    public function editChapter($id) 
    {
        $chapter = Chapter::findOrFail($id);
        return view('admin.edit-chapter', compact('chapter'));
    }

    /**
     * update the chapter
     * @return back method
     */
    public function chapterUpdate($id, Request $request) 
    {

        $chapter = Chapter::findOrFail($id);
        $chapter->title = $request->title;
        $chapter->slug = $request->slug;
        $chapter->description = $request->description;
        $chapter->difficulty = $request->difficulty;
        $chapter->is_paid = $request->is_paid;
        $chapter->estimated_time = $request->estimated_time;
        $chapter->save();

        alert()->success('You have successfully updated the chapter!', 'Updated')->autoclose(2000);
        return back();
    }

    /**
     * delete the chapter by its id
     * @return back method
     */
    public function chapterDelete(Request $request) 
    {
        $chapter = Chapter::findOrFail($request->chapter);
        $chapter->delete();

        alert()->success('You have successfully deleted the chapter!', 'Deleted')->autoclose(2000);
        return back();
    }

    /**
	 * show lessons table for admin
	 * @return \Illuminate\Http\Response
	 */
    public function lessons() 
    {
        $lessons = Lesson::all();
    	return view('admin.lessons', compact('lessons'));
    }

    /**
	 * show view for adding a lesson
	 * @return \Illuminate\Http\Response
	 */
    public function addLesson() 
    {
        $chapters = Chapter::pluck('title', 'id');
    	return view('admin.add-lesson', compact('chapters'));
    }

    /**
     * handle form request for creating lesson
     * @return \Illuminate\Http\Response
     */
    public function lessonCreate(Request $request) 
    {
        $this->validate($request, ['title' => 'required|max:255', 'slug' => 'required|max:255|unique:lessons', 'description' => 'required|min:6', 'difficulty' => 'required', 'is_paid' => 'required', 'estimated_time' => 'required']);

        //creating new lesson
        $lesson = new Lesson();
        $lesson->chapter_id = $request->chapter_id;
        $lesson->title = $request->title;
        $lesson->slug = $request->slug;
        $lesson->description = $request->description;
        $lesson->content = $request->content;
        $lesson->content_vn = $request->content_vn;
        $lesson->difficulty = $request->difficulty;
        $lesson->is_paid = $request->is_paid;
        $lesson->estimated_time = $request->estimated_time;

        if($request->videos){
            $videoPath = request()->file('videos')->store('lessons/'.$request->slug.'/videos', 's3');
            $lesson->video_url = $videoPath;   
        }
        if($request->audios){
            $audioPath = request()->file('audios')->store('lessons/'.$request->slug.'/audios', 's3');
            $lesson->audio_url = $audioPath;    
        }
        
        Auth::user()->lessons()->save($lesson);

        alert()->success('You have successfully created the lesson!', 'Created')->autoclose(2000);
        return back();
    }

    /**
     * show view for editing the lesson
     * @return \Illuminate\Http\Response
     */
    public function editLesson($id) 
    {
        $chapters = Chapter::pluck('title', 'id');
        $lesson = Lesson::findOrFail($id);
        return view('admin.edit-lesson', compact('lesson', 'chapters'));
    }

    /**
     * update the lesson
     * @return back method
     */
    public function lessonUpdate($id, Request $request) 
    {

        $lesson = Lesson::findOrFail($id);
        $lesson->chapter_id = $request->chapter_id;
        $lesson->title = $request->title;
        $lesson->slug = $request->slug;
        $lesson->description = $request->description;
        $lesson->content = $request->content;
        $lesson->content_vn = $request->content_vn;
        $lesson->difficulty = $request->difficulty;
        $lesson->is_paid = $request->is_paid;
        $lesson->estimated_time = $request->estimated_time;

        if($request->videos){
            $videoPath = request()->file('videos')->store('lessons/'.$request->slug.'/videos', 's3');
            $lesson->video_url = $videoPath;   
        }
        if($request->audios){
            $audioPath = request()->file('audios')->store('lessons/'.$request->slug.'/audios', 's3');
            $lesson->audio_url = $audioPath;    
        }
        
        $lesson->save();

        alert()->success('You have successfully updated the lesson!', 'Updated')->autoclose(2000);
        return back();
    }

    /**
     * delete the lesson by its id
     * @return back method
     */
    public function lessonDelete(Request $request) 
    {
        $lesson = Lesson::findOrFail($request->lesson);
        $lesson->delete();

        alert()->success('You have successfully deleted the lesson!', 'Deleted')->autoclose(2000);
        return back();
    }

    /**
	 * show tests table for admin
	 * @return \Illuminate\Http\Response
	 */
    public function tests() 
    {
        $tests = Test::all();
    	return view('admin.tests', compact('tests'));
    }

    /**
	 * show view for adding a test
	 * @return \Illuminate\Http\Response
	 */
    public function addTest() 
    {
        $chapters = Chapter::pluck('title', 'id');
    	return view('admin.add-test', compact('chapters'));
    }

    /**
     * handle form request for creating test
     * @return \Illuminate\Http\Response
     */
    public function testCreate(Request $request) 
    {
        $this->validate($request, ['name' => 'required|max:255', 'slug' => 'required|max:255|unique:lessons', 'description' => 'required|min:6', 'difficulty' => 'required', 'is_paid' => 'required', 'estimated_time' => 'required']);

        //creating new lesson
        $test = new Test();
        $test->chapter_id = $request->chapter_id;
        $test->name = $request->name;
        $test->slug = $request->slug;
        $test->description = $request->description;
        $test->difficulty = $request->difficulty;
        $test->is_paid = $request->is_paid;
        $test->estimated_time = $request->estimated_time;        
        Auth::user()->tests()->save($test);

        alert()->success('You have successfully created the test!', 'Created')->autoclose(2000);
        return back();
    }

    /**
     * show view for editing the test
     * @return \Illuminate\Http\Response
     */
    public function editTest($id) 
    {
        $test = Test::findOrFail($id);
        $chapters = Chapter::pluck('title', 'id');
        return view('admin.edit-test', compact('test', 'chapters'));
    }

    /**
     * update the test
     * @return back method
     */
    public function testUpdate($id, Request $request) 
    {
        $test = Test::findOrFail($id);
        $test->chapter_id = $request->chapter_id;
        $test->name = $request->name;
        $test->slug = $request->slug;
        $test->description = $request->description;
        $test->difficulty = $request->difficulty;
        $test->is_paid = $request->is_paid;
        $test->estimated_time = $request->estimated_time;   

        $test->save();

        alert()->success('You have successfully updated the test!', 'Updated')->autoclose(2000);
        return back();
    }

    /**
     * delete the test by its id
     * @return back method
     */
    public function testDelete(Request $request) 
    {
        $test = Test::findOrFail($request->test);
        $test->delete();

        alert()->success('You have successfully deleted the test!', 'Deleted')->autoclose(2000);
        return back();
    }

    /**
     * show test questions table for admin
     * @return \Illuminate\Http\Response
     */
    public function testQuestions($id) 
    {
        $test = Test::findOrFail($id);
        return view('admin.questions', compact('test'));
    }

    /**
     * show view for adding a question
     * @return \Illuminate\Http\Response
     */
    public function addQuestion($id) 
    {
        $test = Test::findOrFail($id);
        return view('admin.add-question', compact('test'));
    }

    /**
     * handle form request for creating specific test's questions
     * @return \Illuminate\Http\Response
     */
    public function questionCreate($id, Request $request) 
    {

        $this->validate($request, ['question' => 'required']);

        $test = Test::findOrFail($id);
        $question = new Question();
        // $question->type = $request->type;
        $question->question = $request->question;
        if($request->audio){
            $audioPath = request()->file('audio')->store('tests/'.$test->id.'/audio', 's3');
            $question->audio = $audioPath;    
        }
        $test->questions()->save($question);
        
        alert()->success('You have successfully created the question!', 'Created')->autoclose(2000);
        return back();
    }

    /**
     * show view for editing the question for specific test
     * @return \Illuminate\Http\Response
     */
    public function editQuestion($id) 
    {
        $question = Question::findOrFail($id);
        return view('admin.edit-question', compact('question'));
    }

    /**
     * update the question
     * @return back method
     */
    public function questionUpdate($id, Request $request) 
    {
        $question = Question::findOrFail($id);

        // $question->type = $request->type;
        $question->question = $request->question;
        if($request->audio){
            $audioPath = request()->file('audio')->store('tests/'.$test->id.'/audio', 's3');
            $question->audio = $audioPath;    
        }   

        $question->save();

        alert()->success('You have successfully updated the question!', 'Updated')->autoclose(2000);
        return back();
    }

    /*
    * delete the question by its id
    * @return back method
    */
    public function questionDelete(Request $request) {
        $question = Question::findOrFail($request->question);
        $question->delete();

        alert()->success('You have successfully deleted the question!', 'Deleted')->autoclose(2000);
        return back();
    }

    /**
     * show question's choices table for admin
     * @return \Illuminate\Http\Response
     */
    public function questionChoices($id)
    {
        $question = Question::findOrFail($id);
        return view('admin.choices', compact('question'));
    }

    /**
     * show view for adding a choice
     * @return \Illuminate\Http\Response
     */
    public function addChoice($id)
    {
        $question = Question::findOrFail($id);
        return view('admin.add-choice', compact('question'));
    }

    /**
     * handle form request for creating specific questions's choice
     * @return \Illuminate\Http\Response
     */
    public function choiceCreate($id, Request $request) 
    {

        $this->validate($request, ['choice' => 'required|max:255', 'is_right_choice' => 'required']);

        $question = Question::findOrFail($id);
        $choice = new Choice();
        $choice->choice = $request->choice;
        $choice->is_right_choice = $request->is_right_choice;
        $question->choices()->save($choice);
        
        alert()->success('You have successfully created the choice!', 'Created')->autoclose(2000);
        return back();
    }

    /**
     * show view for editing the choice
     * @return \Illuminate\Http\Response
     */
    public function editChoice($id) 
    {
        $choice = Choice::findOrFail($id);
        return view('admin.edit-choice', compact('choice'));
    }

    /**
     * update the choice
     * @return back method
     */
    public function choiceUpdate($id, Request $request) 
    {
        $choice = Choice::findOrFail($id);

        $choice->choice = $request->choice;
        $choice->is_right_choice = $request->is_right_choice;  
        $choice->save();

        alert()->success('You have successfully updated the choice!', 'Updated')->autoclose(2000);
        return back();
    }

    /**
    * delete the choice by its id
    * @return back method
    */
    public function choiceDelete(Request $request) {
        $choice = Choice::findOrFail($request->choice);
        $choice->delete();

        alert()->success('You have successfully deleted the choice!', 'Deleted')->autoclose(2000);
        return back();
    }

}
