<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Http\Requests;
use App\Lesson;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Newsletter;

class PagesController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
    }
    
	/**
	 * Show homepage
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $chapters = Chapter::latest()->limit(12)->get();
        $lessons = Lesson::latest()->limit(6)->get();
        $tests = Test::latest()->limit(6)->get();
        
    	return view('pages.home', compact('chapters','lessons', 'tests'));
    }

    /**
     * show page about pricing
     * @return \Illuminate\Http\Response
     */
    public function pricing(){
    	return view('pages.pricing');
    }

     /**
     * show contact page
     * @return \Illuminate\Http\Response
     */
    public function contact(){
    	return view('pages.contact');
    }

    /**
     * show search results
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $searchText = $request->search;
        if($searchText == ''){
            $searchText = 'empty';
        }else{
            $searchText = $searchText;
        }
        $lessons = Lesson::search($searchText)->get();
        $tests = Test::search($searchText)->get();
        $chapters = Chapter::search($searchText)->get();
        return view('pages.search', compact('lessons', 'tests', 'chapters', 'searchText'));
    }

    /**
     * Change Language
     * @param  Request $request
     * @return [type]           [description]
     */
    public function lang(Request $request)
    {
        Session::set('locale', $request->lang);
        return back();
    }

    /**
     * Subscribe newsletter
     * @param  Request $request
     * @return [type]           [description]
     */
    public function newsletter(Request $request)
    {
        Newsletter::subscribe($request->news_email);
        alert()->success('You have successfully subscribed for newsletters!', 'Subscribed')->autoclose(3500);
        return back();
    }
}
