<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
	/**
     * constructor for UsersController
     */
    public function __construct()
    {
        $this->middleware('locale');
        $this->middleware('auth');
    }
    
    /**
     * show user's settings
     * @return \Illuminate\Http\Response
     */
    public function settings(){
        $user = User::findOrFail(Auth::id());
        return view('pages.settings', compact('user'));
    }

    /**
     * handle request for avatar change
     * @return method redirecting to previous page
     */
    public function avatarUpdate(Request $request){
        $avatar = request()->file('avatar')->store('avatars/'.Auth::id().'/avatar', 's3');
        $user = User::findOrFail(Auth::id());
        $user->update(['avatar' => $avatar]);
        return back();
    }

    /**
     * handle request for personal info change
     * @return method redirecting to previous page
     */
    public function personalUpdate(Request $request){
        $this->validate($request, ['name' => 'required|max:255']);
        $user = User::findOrFail(Auth::id());
        $user->update($request->all());
        return back();
    }

    /**
     * handle request for password change
     * @return method redirecting to previous page
     */
    public function passwordUpdate(Request $request){
        $this->validate($request, ['password' => 'required|min:6', 'new_password' => 'required|min:6|confirmed']);
        $user = User::findOrFail(Auth::id());

        if(Hash::check($request->password, $user->password)){
            $user->update(['password' => bcrypt($request->new_password)]);
            return back();
        } else {
            return back()->withErrors(['password' => trans('errors.wrong_pw')]);
        }
    }

    /**
     * handle request for email change
     * @return method redirecting to previous page
     */
    public function emailUpdate(Request $request){
        $this->validate($request, ['email' => 'required|email|max:255|unique:users']);
        $user = User::findOrFail(Auth::id());
        $user->update($request->all());
        return back();
    }

    /**
     * handle request for username change
     * @return method redirecting to previous page
     */
    public function usernameUpdate(Request $request){
        $this->validate($request, ['username' => 'required|min:5|unique:users' ]);
        $user = User::findOrFail(Auth::id());
        $user->update($request->all());
        return back();
    }

    /**
     * handle request for payment
     * @return method redirecting to previous page
     */
    public function payment(Request $request){
        $creditCardToken = $request->stripeToken;
        $user = User::findOrFail(Auth::id());
        $user->newSubscription('main', 'month')->create($creditCardToken);
        return back();
    }

    /**
     * handle request for cancelling payment
     * @return method redirecting to previous page
     */
    public function cancelPayment(Request $request){
        $user = User::findOrFail(Auth::id());
        $user->subscription('main')->cancel();
        return back();
    }

    /**
     * handle request for resuming payment
     * @return method redirecting to previous page
     */
    public function resumePayment(Request $request){
        $user = User::findOrFail(Auth::id());
        $user->subscription('main')->resume();
        return back();
    }

}
