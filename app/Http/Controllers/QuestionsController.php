<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Choice;
use App\Http\Requests;
use App\Submission;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionsController extends Controller
{
     /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
    }

    /**
	 * Show specific test's questions
	 * @return \Illuminate\Http\Response
	 */
    public function index($slug){
    	$test = Test::slug($slug)->firstOrFail();

        /*
         * If the test is 'paid' user must be logged in to do tests (have paid for register)
         */
        if($test->is_paid){
            if(Auth::check()){
                if(auth()->user()->subscribed('main')){
                    return view('pages.questions', compact('test'));
                }else{
                    return redirect('/login');    
                }
            }else {
                return redirect('/login');   
            }
        }else{
            return view('pages.questions', compact('test'));
        }

    }

    /**
     * evaluate user's answers and store them in DB
     * @param  Request $request
     * @return results
     */
    public function evaluate($slug, Request $request)
    {
        $test = Test::slug($slug)->firstOrFail();

        $correctChoices = array();
        $userAnswers = array();
        $rightChoices = Choice::where('is_right_choice', 1)->get();

        foreach($rightChoices as $rightChoice){
            array_push($correctChoices ,$rightChoice->id);
        }
        $rightAnswersCount = 0;

        if(Auth::check()){
            //save test id and user id to submission first
            $submission = new Submission();
            $submission->test_id = $test->id;
            Auth::user()->submissions()->save($submission);
            $submissionId = $submission->id;
        }

        foreach($test->questions as $question){
            $answerID = $request['question_'.$question->id];
            $choice = Choice::find($answerID);
            if(is_null($choice)){
                $isRightChoice= 0;
            }else{
                $isRightChoice = $choice->is_right_choice;
            }

            // add answers to DB
            if(Auth::check()){
            //then save other infos to answers
            $answer = new Answer();
            $answer->question_id = $question->id;
            $answer->choice_id = $answerID;
            $answer->is_right = $isRightChoice;
            $answer->submission_id = $submissionId;
            $answer->save();

            }

            if($isRightChoice){
                $rightAnswersCount++;
            }
            array_push($userAnswers, $answerID);


        }

        $numOfQuestions = count($test->questions);

        return view('pages.results', compact('test', 'userAnswers', 'correctChoices', 'rightAnswersCount', 'numOfQuestions'));
    }
}
