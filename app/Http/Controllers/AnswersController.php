<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AnswersController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
        $this->middleware('auth');
    }

    public function index(){
    	$user = User::findOrFail(Auth::id());
    	return view('pages.sent-tests');
    }
}
