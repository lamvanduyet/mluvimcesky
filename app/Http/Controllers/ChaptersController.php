<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Http\Requests;
use Illuminate\Http\Request;

class ChaptersController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('locale');
    }

    /**
	 * Show all chapters
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        $chapters = Chapter::latest()->paginate(9);
    	return view('pages.chapters', compact('chapters'));
    }

        /**
     * Show specific chapter
     * @return \Illuminate\Http\Response
     */
    public function chapter($slug){

        $chapter = Chapter::slug($slug)->firstOrFail();

        /*
         * If the chapter is paid user must be logged in (have paid for register)
         */
        
        if($chapter->is_paid){
            if(Auth::check()){
                if(auth()->user()->subscribed('main')){
                    return view('pages.chapter', compact('chapter'));
                }else{
                    return redirect('/login');    
                }
            }else {
                return redirect('/login');   
            }
        }else{
            return view('pages.chapter', compact('chapter'));
        }
        
    }
}
