<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
	 * relationship with Test model
	 * @return belongsTo relationship
	 */
    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    /**
     * relationship with Choice model
     * @return hasMany relationship
     */
    public function choices()
    {
        return $this->hasMany('App\Choice');
    }

    /**
     * relationship with Answer model
     * @return hasMany relationship
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
