<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use Searchable;

    /**
	 * attributes that can be mass assigned
	 * @var [type]
	 */
	protected $fillable = [
        'title', 'slug', 'description', 'difficulty', 'is_paid', 'estimated_time'
    ];

    /**
	 * global query scope
	 * @param  $query
	 * @param  $slug
	 * @return query scope
	 */
	public function scopeSlug($query, $slug)
    {
        $query->where('slug', $slug);
    }

    /**
	 * relationship with User model
	 * @return belongsTo relationship
	 */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * relationship with Lesson model
     * @return hasMany relationship
     */
    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    /**
     * relationship with Test model
     * @return hasMany relationship
     */
    public function tests()
    {
        return $this->hasMany('App\Test');
    }
}
