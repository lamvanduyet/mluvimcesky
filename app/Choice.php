<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    /**
	 * relationship with Question model
	 * @return belongsTo relationship
	 */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * relationship with Answer model
     * @return hasMany relationship
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
