<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
	 * relationship with Question model
	 * @return belongsTo relationship
	 */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
	 * relationship with Choice model
	 * @return belongsTo relationship
	 */
    public function choice()
    {
        return $this->belongsTo('App\Choice');
    }

    /**
     * relationship with Submission model
     * @return belongsTo relationship
     */
    public function submission()
    {
        return $this->belongsTo('App\Submission');
    }
}
