<?php

namespace App;

use Laravel\Cashier\Billable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Stripe tax
     * @return tax percent
     */
    public function taxPercentage() {
        return 20;
    }

    /**
     * relationship with Chapter model
     * @return hasMany relationship
     */
    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }

    /**
     * relationship with Lesson model
     * @return hasMany relationship
     */
    public function lessons()
    {
        return $this->hasMany('App\Lesson');
    }

    /**
     * relationship with Test model
     * @return hasMany relationship
     */
    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    /**
     * relationship with Comment model
     * @return hasMany relationship
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * relationship with Submission model
     * @return hasMany relationship
     */
    public function submissions()
    {
        return $this->hasMany('App\Submission');
    }
}
