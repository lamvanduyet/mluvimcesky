<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
	use Searchable;

	/**
	 * attributes that can be mass assigned
	 * @var [type]
	 */
	protected $fillable = [
        'title', 'slug', 'description', 'content', 'content_vn', 'difficulty', 'is_paid', 'estimated_time'
    ];

	/**
	 * global query scope
	 * @param  $query
	 * @param  $slug
	 * @return query scope
	 */
	public function scopeSlug($query, $slug)
    {
        $query->where('slug', $slug);
    }

	/**
	 * relationship with User model
	 * @return belongsTo relationship
	 */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
	 * relationship with Chapter model
	 * @return belongsTo relationship
	 */
    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }
}
