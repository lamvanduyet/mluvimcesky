<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
|Auth Routes
|--------------------------------------------------------------------------
*/
Auth::routes();

/*
|--------------------------------------------------------------------------
| Web Routes for public
|--------------------------------------------------------------------------
|
*/
Route::get('/home', 'PagesController@index');
Route::get('/', 'PagesController@index');
Route::get('/chapters', 'ChaptersController@index');
Route::get('/chapters/{slug}', 'ChaptersController@chapter');
Route::get('/lessons', 'LessonsController@index');
Route::get('/lessons/{slug}', 'LessonsController@lesson');
Route::get('/etests', 'TestsController@index');
Route::get('/submissions', 'SubmissionsController@index');
Route::get('/tests/{slug}', 'TestsController@test');
Route::get('/tests/{slug}/questions', 'QuestionsController@index');
Route::post('tests/{slug}/questions/results', 'QuestionsController@evaluate');
Route::get('/contact', 'PagesController@contact');
Route::get('/pricing', 'PagesController@pricing');
Route::get('/search', 'PagesController@search');
/*
|--------------------------------------------------------------------------
| Lang and Newsletter
|--------------------------------------------------------------------------
*/
Route::get('/lang', 'PagesController@lang');
Route::post('/newsletter', 'PagesController@newsletter');

/*
|--------------------------------------------------------------------------
| User routes (settings)
|--------------------------------------------------------------------------
*/
Route::get('/settings', 'UsersController@settings');
Route::post('settings/avatar/update', 'UsersController@avatarUpdate');
Route::post('settings/personal/update', 'UsersController@personalUpdate');
Route::post('settings/password/update', 'UsersController@passwordUpdate');
Route::post('settings/email/update', 'UsersController@emailUpdate');
Route::post('settings/username/update', 'UsersController@usernameUpdate');
Route::post('settings/payment', 'UsersController@payment');
Route::post('settings/cancelPayment', 'UsersController@cancelPayment');
Route::post('settings/resumePayment', 'UsersController@resumePayment');
Route::get('user/invoice/{invoice}', function (Request $request, $invoiceId) {
    return $request->user()->downloadInvoice($invoiceId, [
        'vendor'  => 'MluvimCesky.cz',
        'product' => 'Odber',
    ]);
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

// Routes for tables
Route::get('admin', 'AdminController@index');
Route::get('admin/dashboard', 'AdminController@dashboard');
Route::get('admin/chapters', 'AdminController@chapters');
Route::get('admin/lessons', 'AdminController@lessons');
Route::get('admin/tests', 'AdminController@tests');
Route::get('admin/tests/{id}/questions', 'AdminController@testQuestions');
Route::get('admin/users', 'AdminController@users');
Route::get('admin/questions/{id}/choices', 'AdminController@questionChoices');


// Chapter CRUD
Route::get('admin/add-chapter', 'AdminController@addChapter'); // view blade for adding chapters
Route::post('admin/chapters/create', 'AdminController@chapterCreate'); // handle request for creating a chapter
Route::get('admin/chapters/{id}/edit', 'AdminController@editChapter'); // view blade for editing the specific chapter
Route::post('admin/chapters/{id}/update', 'AdminController@chapterUpdate'); // handle request for updating the chapter
Route::post('admin/chapters/delete', 'AdminController@chapterDelete'); // handle request for deleting the specific chapter

// Lesson CRUD
Route::get('admin/add-lesson', 'AdminController@addLesson'); // view blade for adding lessons
Route::post('admin/lessons/create', 'AdminController@lessonCreate'); // handle request for creating a lesson
Route::get('admin/lessons/{id}/edit', 'AdminController@editLesson'); // view blade for editing the specific lesson
Route::post('admin/lessons/{id}/update', 'AdminController@lessonUpdate'); // handle request for updating the lesson
Route::post('admin/lessons/delete', 'AdminController@lessonDelete'); // handle request for deleting the specific lesson

// Test CRUD
Route::get('admin/add-test', 'AdminController@addTest'); // view blade for adding tests
Route::post('admin/tests/create', 'AdminController@testCreate'); // handle request for creating a test
Route::get('admin/tests/{id}/edit', 'AdminController@editTest'); // view blade for editing the test
Route::post('admin/tests/{id}/update', 'AdminController@testUpdate'); // handle request for updating the test
Route::post('admin/tests/delete', 'AdminController@testDelete'); // handle request for deleting the test

// Question CRUD
Route::get('admin/tests/{id}/add-question', 'AdminController@addQuestion'); // view blade for adding questions
Route::post('admin/tests/{id}/question-create', 'AdminController@questionCreate'); // handle request for creating a question
Route::get('admin/questions/{id}/edit', 'AdminController@editQuestion'); // view blade for editing the question
Route::post('admin/questions/{id}/update', 'AdminController@questionUpdate'); // handle request for updating the question
Route::post('admin/questions/delete', 'AdminController@questionDelete'); // handle request for deleting the question

// Choice CRUD
Route::get('admin/questions/{id}/add-choice', 'AdminController@addChoice'); // view blade for adding choices
Route::post('admin/questions/{id}/choice-create', 'AdminController@choiceCreate'); // handle request for creating a choice
Route::get('admin/choices/{id}/edit', 'AdminController@editChoice'); // view blade for editing the choice
Route::post('admin/choices/{id}/update', 'AdminController@choiceUpdate'); // handle request for updating the choice
Route::post('admin/choices/delete', 'AdminController@choiceDelete'); // handle request for deleting the choice


