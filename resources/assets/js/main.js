// STRIPE PAYMENT

$(document).ready(function() {
    // This identifies your website in the createToken call below
    Stripe.setPublishableKey('pk_test_g9RhNwaAOKbhhPOczIvBXJtM');

    $('.payment-errors').hide();

    $('#payment-form button').on('click', function() {
        var form = $('#payment-form');
        var submit = form.find('button');
        var submitInitialText = submit.text();

        submit.attr('disabled', 'disabled').text('Just one moment..');

        Stripe.card.createToken(form, function(status, response) {
            var token;
            var errorMessages = {
                incorrect_number: "Incorrect card number.",
                invalid_number: "Invalid card number.",
                invalid_expiry_month: "Invalid expiry month.",
                invalid_expiry_year: "Invalid expiry year.",
                invalid_cvc: "Invalid CVC.",
                expired_card: "Expired Card.",
                incorrect_cvc: "Incorrect CVC.",
                card_declined: "Card declined.",
                missing: "There is no card on a customer that is being charged.",
                processing_error: "An error occurred while processing the card.",
                rate_limit:  "An error occurred due to requests hitting the API too quickly. Please let us know if you're consistently running into this error."
            };

            if(response.error && response.error.type == 'card_error') {
                $( '.payment-errors' ).text( errorMessages[ response.error.code ]).show();
                //form.find('.payment-errors').text(response.error.message).show();
                submit.removeAttr('disabled');
                submit.text(submitInitialText);
            } else {
                token = response.id;
                form.append($('<input type="hidden" name="stripeToken">').val(token));
                //form.get(0).submit();
                form.submit();
            }
        });
    });
});

$(document).ready(function() {
    $("#cardNumber").keydown(function(){

        $(this).validateCreditCard(function(result) {
            if(result.card_type) {
                $('.credit-card').attr('src', '../img/credit-cards/' + result.card_type.name + '.png');
                if(result.card_type.name == 'diners_club_carte_blanche' || result.card_type.name == 'diners_club_international') {
                    $('.credit-card').attr('src', '../img/credit-cards/diners.png');
                }
                if(result.valid) {
                    $(this).closest('.form-group').addClass('has-success');
                } else {
                    $(this).closest('.form-group').removeClass('has-success');
                }
            } else {
                $('.credit-card').attr('src', '../img/credit-cards/credit.png');
            }
        });
    });

    $(".cardNumber").keydown(function(){
            $(this).validateCreditCard(function(result) {
                if(result.card_type) {
                    $('.credit-card.plus').attr('src', '../img/credit-cards/' + result.card_type.name + '.png');
                    if(result.card_type.name == 'diners_club_carte_blanche' || result.card_type.name == 'diners_club_international') {
                        $('.credit-card.plus').attr('src', '../img/credit-cards/diners.png');
                    }
                    if(result.valid) {
                        $(this).closest('.form-group').addClass('has-success');
                    } else {
                        $(this).closest('.form-group').removeClass('has-success');
                    }
                } else {
                    $('.credit-card').attr('src', '../img/credit-cards/credit.png');
                }
            });
        });




    var year = new Date().getFullYear() % 100;
    var thisMonth = new Date().getMonth() + 1;

    $('#exp-month').on('input',function() {
        var input = parseInt($(this).val());

        if ($(this).val().length == 2 && parseInt($('#exp-year').val()) == year && input < thisMonth) {
            $(this).closest('.form-group').removeClass('has-success');
        } else if ($(this).val().length == 2 && parseInt($('#exp-year').val()) >= year && input > 0 && input < 13) {
            $(this).closest('.form-group').addClass('has-success');
        } else {
            $(this).closest('.form-group').removeClass('has-success');
        }
    });

    $('.exp-month').on('input',function() {
        var input = parseInt($(this).val());
        console.log(input,parseInt($('.exp-year').val()), thisMonth);

        if ($(this).val().length == 2 && parseInt($('.exp-year').val()) == year && input < thisMonth) {
            $(this).closest('.form-group').removeClass('has-success');
        } else if ($(this).val().length == 2 && parseInt($('.exp-year').val()) >= year && input > 0 && input < 13) {
            $(this).closest('.form-group').addClass('has-success');
        } else {
            $(this).closest('.form-group').removeClass('has-success');
        }
    });

    $('#exp-year').on('input',function() {
        var input = parseInt($(this).val());

        if (input == year && parseInt($('#exp-month').val()) < thisMonth) {
            $(this).closest('.form-group').removeClass('has-success');
            $(this).closest('.form-group').addClass('has-error');
        } else if (($('#exp-year').val().length == 2 && input >= year && parseInt($('#exp-month').val()) < 13 && parseInt($('#exp-month').val()) > 0)) {
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }else {
            $(this).closest('.form-group').removeClass('has-success');
            $(this).closest('.form-group').addClass('has-error');
        }
    });

    $('.exp-year').on('input',function() {
        var input = parseInt($(this).val());

        if (input == year && parseInt($('.exp-month').val()) < thisMonth) {
            $(this).closest('.form-group').removeClass('has-success');
            $(this).closest('.form-group').addClass('has-error');
        } else if (($('.exp-year').val().length == 2 && input >= year && parseInt($('.exp-month').val()) < 13 && parseInt($('.exp-month').val()) > 0)) {
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }else {
            $(this).closest('.form-group').removeClass('has-success');
            $(this).closest('.form-group').addClass('has-error');
        }
    });

    $('#cvc,.cvc').on('input',function() {
        if ($(this).val().length == 3) {
            $(this).closest('.form-group').addClass('has-success');
        } else {
            $(this).closest('.form-group').removeClass('has-success');
        }
    });


});