<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Pricing
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for pricing for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Ceník',
   'price' => 'Jenom 120Kč/měsíčně',
   'text' => 'Chcete se naučit hezky česky? U nás jenom 120Kč měsíčně! Investujte do sebe a naučte se česky. Pokud si to rozmyslíte a nebudete chtít platit, stačí zmáčknout jedním tlačítkem a hotovo.',

];