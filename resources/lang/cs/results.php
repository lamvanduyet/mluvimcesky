<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Tests
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for tests for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Výsledky',
   'questions' => '{0} otázek| {1} otázky|[2,Inf] otázek',

];