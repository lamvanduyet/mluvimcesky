<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Navbar
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various navbar
    | messages that we need to display to the user.
    |
    */
   'learning' => 'E-Learning',
   'lessons' => 'Lekce',
   'tests' => 'Testy',
   'pricing' => 'Ceník',
   'contact' => 'Kontakt',
   'login' => 'Přihlásit',
   'register' => 'Registrace',
   'logout' => 'Odhlásit se',
   'join' => 'Přidej se',
   'settings' => 'Nastavení',
   'search' => 'Hledání',
   'submissions' => 'Odevzdané testy',
   'chapters' => 'Kapitoly'

];