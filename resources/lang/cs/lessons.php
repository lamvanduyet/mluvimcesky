<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Lessons
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for lessons for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Lekce',
   '404' => 'Nebyla nalezena žádná lekce.',
   'featured' => 'Vybrané lekce',

];