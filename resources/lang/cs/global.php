<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various global
    | messages that we need to display to the user.
    |
    */
   'free' => 'Zdarma',
   'paid' => 'Placené',
   'estimated_time' => 'Předpokládaný čas',

];