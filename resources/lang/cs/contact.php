<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Contact
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for contact for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Kontakt',
   'text' => 'email: info@mluvimcesky.cz',

];