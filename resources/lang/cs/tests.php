<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Tests
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for tests for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Testy',
   '404' => 'Nebyl nalezen žádný test.',
   'featured' => 'Vybrané testy',
   'start' => 'Začít test',
   'end' => 'Ukončit test',
   'again' => 'Opakovat test',
   'questions_count' => '{0} otázek|{1} otázka|[2,4] otázky|[5,Inf] otázek'

];