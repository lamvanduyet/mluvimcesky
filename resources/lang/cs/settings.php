<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Settings
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for settings for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Nastavení',
   'personal_settings' => 'Osobní údaje',
   'password_settings' => 'Změna hesla',
   'email_settings' => 'Změna E-mailu',
   'username_settings' => 'Změna Uživatelského jména',
   'payment' => 'Platba',
   'upload_avatar' => 'Nahrát obrázek',
   'name' => 'Jméno',
   'username' => 'Uživatelské jméno',
   'old_pw' => 'Staré heslo',
   'new_pw' => 'Nové heslo',
   'confirm_new' => 'Znovu nové heslo',
   'card_number' => 'Číslo karty',
   'expiration' => 'Platnost karty (MM/RR)',
   'zip' => 'PSČ',
   'subscribe' => 'Přihlásit se k odběru',
   'subscribed' => 'Jste již v odběru.',
   'cancelled' => 'Už jste zrušili odběr.',
   'grace_period' => 'Zrušili jste odběr, ale jste stále v respiru.',
   'download' => 'Stáhnout',
   'cancel' => 'Zrušit odběr',
   'resume' => 'Obnovit odběr',
   'from' => 'Od',
   'total' => 'Celkem'


];