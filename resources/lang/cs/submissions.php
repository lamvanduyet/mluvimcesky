<?php

return [
	  /*
    |--------------------------------------------------------------------------
    | Language Lines For Submissions
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for submissions for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Odevzdané testy',
   'test_name' => 'Název testu',
   'points' => 'Dosaženo bodů',
   'total_points' => 'Max bodů',
   'results' => 'Úspěšnost',
   'date' => 'Odevzdáno'

];