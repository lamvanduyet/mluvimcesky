	<?php
  
  return [
  /*
    |--------------------------------------------------------------------------
    | Language Lines For Lessons
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for lessons for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Kapitoly',
   '404' => 'Nebyly nalezeny žádné kapitoly.',
   'featured' => 'Vybrané kapitoly'
   ];