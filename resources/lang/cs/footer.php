<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Footer
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various footer
    | messages that we need to display to the user.
    |
    */
   'home' => 'Úvodní stránka',
   'lessons' => 'Lekce',
   'tests' => 'Testy',
   'pricing' => 'Ceník',
   'contact' => 'Kontakt',
   'login' => 'Přihlásit',
   'register' => 'Registrace',
   'logout' => 'Odhlásit',
   'newsletter' => 'Přihlásit se k newsletteru',
   'subscribe' => 'Odebírat',
   'navigation' => 'Navigace',
   'auth' => 'Autentizace',
   'mluvimcesky' => 'Chcete se naučit češtinu? Jste na správném místě. Nabízíme kvalitní materiály pro e-learning češtiny, od lekcí k online testům, které Vás připraví na různé zkoušky, ale také na běžnou komunikaci.'

];