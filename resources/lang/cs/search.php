<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Search
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for search for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Hledání',
   'results_for' => 'Výsledky hledání pro',
   'chapters_found' => '{0} Žádná kapitola|{1} Kapitola nalezena | [2,4] Kapitoly nalezeny| [5,Inf] Kapitol nalezeno',
   'lessons_found' => '{0} Žádné lekce| [1,4] Lekce nalezena| [5,Inf] Lekcí nalezeno',
   'tests_found' => '{0} Žádný test|{1} Test nalezen | [2,4] Testy nalezeny| [5,Inf] Testů nalezeno',

];