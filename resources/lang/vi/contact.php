<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Contact
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for contact for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Liên hệ',
   'text' => 'E-mail: info@mluvimcesky.cz',

];