<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Pricing
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for pricing for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Bảng giá',
   'price' => 'Chỉ 120Kč/tháng',
   'text' => 'Bạn muốn học tiếng Séc? Ở chỗ chúng tôi bạn chỉ trả 120Kč/tháng. Bạn hãy đầu tư vào mình và bắt đầu học tiếng Séc. Nếu bạn không hài lòng bạn cò thể huỷ chỉ cần một nút bấm.',

];