<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Navbar
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various navbar
    | messages that we need to display to the user.
    |
    */
   'learning' => 'E-Learning',
   'lessons' => 'Bài học',
   'tests' => 'Bài kiểm tra',
   'pricing' => 'Bảng giá',
   'contact' => 'Liên hệ',
   'login' => 'Đăng nhập',
   'register' => 'Đăng ký',
   'logout' => 'Thoát',
   'join' => 'Tham gia',
   'settings' => 'Điều chỉnh',
   'search' => 'Tìm kiếm',
   'submissions' => 'Bài kiểm tra đã nộp',
   'chapters' => 'Chương'

];