<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Settings
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for settings for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Điều chỉnh',
   'personal_settings' => 'Dữ liệu cá nhân',
   'password_settings' => 'Đổi mật khẩu',
   'email_settings' => 'Đổi E-mail',
   'username_settings' => 'Đổi nickname',
   'payment' => 'Thanh toán',
   'upload_avatar' => 'Tải ảnh',
   'name' => 'Tên',
   'username' => 'Nickname',
   'old_pw' => 'Mật khẩu cũ',
   'new_pw' => 'Mật khẩu mới',
   'confirm_new' => 'Mật khẩu mới lại',
   'card_number' => 'Số thẻ',
   'expiration' => 'Ngày hết hạn (MM/RR)',
   'zip' => 'PSČ',
   'subscribe' => 'Đăng ký trả dài hạn.',
   'subscribed' => 'Bạn đã đăng ký trả dài hạn.',
   'cancelled' => 'Bạn đã huỷ đăng ký trả dài hạn',
   'grace_period' => 'Bạn đã huỷ đăng ký trả dài hạn, nhung bạn vẫn ở thời gian ân hạn.',
   'download' => 'Tải về',
   'cancel' => 'Huỷ đăng ký',
   'resume' => 'Đăng ký lại',
   'from' => 'Từ ngày',
   'total' => 'Tổng'


];