<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Search
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for search for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Tìm kiếm',
   'results_for' => 'Kết quả tìm kiếm cho',
   'chapters' => '{0} Không chương nào| [1,4] Chương tìm thấy| [5,Inf] Chương tìm thấy',
   'lessons_found' => '{0} Không bài học nào| [1,4] Bài học tìm thấy| [5,Inf] Bài học tìm thấy',
   'tests_found' => '{0} Không bài kiểm tra nào|{1} Bài kiểm tra tìm thấy| [2,4] Bài kiểm tra tìm thấy| [5,Inf] Bài kiểm tra tìm thấy',

];