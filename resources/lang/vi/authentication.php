<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => 'Đăng nhập',
    'register' => 'Đăng ký',
    'join' => 'Tham gia',
    'remember' => 'Nhớ lại tôi',
    'forgot' => 'Bạn quên mật khẩu?',
    'already_have_acc' => 'Bạn có tài khoản rồi?',
    'name' => 'Tên',
    'username' => 'Nickname',
    'email' => 'E-Mail',
    'password' => 'Mật khẩu',
    'confirm' => 'Mật khẩu lại',
    'reset' => 'Mật khẩu mới',
    'send_password' => 'Gửi mật khẩu mới',
];
