<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Buttons
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for buttons
    | that we need to display to the user.
    |
    */
   'more' => 'Xem tiếp',
   'update' => 'Lưu lại',

];