<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Lessons
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for lessons for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Bài học',
   '404' => 'Không tìm thấy bài học nào.',
   'featured' => 'Bài học được chọn',

];