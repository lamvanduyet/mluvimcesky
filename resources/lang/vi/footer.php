<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Footer
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various footer
    | messages that we need to display to the user.
    |
    */
   'home' => 'Trang chủ',
   'lessons' => 'Bài học',
   'tests' => 'Bài kiểm tra',
   'pricing' => 'Bảng giá',
   'contact' => 'Liên hệ',
   'login' => 'Đăng nhập',
   'register' => 'Đăng ký',
   'logout' => 'Thoát',
   'newsletter' => 'Đăng ký tin',
   'subscribe' => 'Đăng ký',
   'navigation' => 'Menu',
   'auth' => 'Đăng ký/Đăng nhập',
   'mluvimcesky' => 'Bạn muốn học tiếng Séc? Bạn đã đến đúng nôi. Chúng tôi cung cấp tài liệu để học tiếng Séc, từ bài học đến bài kiểm tra online, và chuẩn bị các bạn để đi thi tiếng Séc hoạc để nói chuyện hàng ngày.'

];