<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Tests
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for tests for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Kết quả',
   'questions' => '{0} câu hỏi| {1} câu hỏi|[2,Inf] câu hỏi',

];