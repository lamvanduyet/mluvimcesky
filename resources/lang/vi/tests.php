<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Tests
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for tests for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Bài kiểm tra',
   '404' => 'Không tìm thấy bài kiểm tra nào.',
   'featured' => 'Bài kiểm tra đã được chọn',
   'start' => 'Bắt đầu làm bài kiểm tra',
   'end' => 'Kết thúc bài kiểm tra',
   'again' => 'Làm lại bài kiểm tra',
   'questions_count' => '{0} câu hỏi|{1} câu hỏi|[2,4] câu hỏi|[5,Inf] câu hỏi'

];