<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language Lines For Submissions
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for submissions for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Bài kiểm tra đã nộp',
   'test_name' => 'Tên bài kiểm tra',
   'points' => 'Điểm nhận được',
   'total_points' => 'Điểm tổng cộng',
   'results' => 'Kết quả',
   'date' => 'Ngày nộp'

];