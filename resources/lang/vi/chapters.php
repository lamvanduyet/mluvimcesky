<?php
return [
	/*
    |--------------------------------------------------------------------------
    | Language Lines For Lessons
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for lessons for various
    | messages that we need to display to the user.
    |
    */
   'title' => 'Chương',
   '404' => 'Không tìm thấy chương nào.',
   'featured' => 'Chương được chọn'

];