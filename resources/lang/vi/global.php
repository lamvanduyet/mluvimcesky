<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for various global
    | messages that we need to display to the user.
    |
    */
   'free' => 'Miễn phí',
   'paid' => 'Mất tiền',
   'estimated_time' => 'Thời gian dự kiến',

];