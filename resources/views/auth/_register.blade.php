@extends('layouts.app')

@section('title', 'Register')

@section('content')
<div class="container main-content">
    <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
    <div class="col-md-6">
        <h4 class="section-title">Step 1</h4>
        <div class="panel panel-default card">
          <div class="panel-heading card-heading">Monthly Subscription: 9$</div>
          <div class="panel-body">
            {{-- <h3 class="text-center">Monthly (9$)</h3> --}}
            {{-- <form class="form-horizontal" action=""> --}}
                <div class="form-group">
                <label for="card_number" class="col-md-3 control-label">Card Number</label>
                    <div class="col-md-7">
                        <input type="text" name="card_number" class="form-control" placeholder="Card Number" required>    
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-3">
                <div class="form-group">
                    <div class="col-xs-4">
                        <input type="text" name="month" class="form-control" placeholder="Month" required>      
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="year" class="form-control" placeholder="Year" required>      
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="cvc" class="form-control" placeholder="CVC" required>      
                    </div>
                </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
    </div>

    {{-- <h4 class="section-title">Register</h4> --}}
    <div class="col-md-6 ">
        <h4 class="section-title">Step 2</h4>
            <div class="panel panel-default card">
                <div class="panel-heading card-heading">Register</div>
                <div class="panel-body">
                    {{-- <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }} --}}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-custom">
                                    Join Now
                                </button>
                                {{-- <a class="btn btn-fb" href="">
                                    <i class="fa fa-facebook" aria-hidden="true"></i> Facebook
                                </a> --}}
                            </div>
                        </div>
                    {{-- </form> --}}
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
