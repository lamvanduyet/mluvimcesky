<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Lam Van Duyet">
    <meta name="keywords" content="e-learning češtiny pro cizince, lekce, kurzy, návody, testy, cvičení, nauka českého jazyka, zkouška z českého jazyka na trvalý pobyt, gramatika, skloňování podstatných jmen, tutoriály">
    <meta name="description" content="MluvimCesky.cz - e-learning češtiny pro cizince obsahující lekce, návody a testy">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MluvímČesky') }} &middot; @yield('title') &middot; E-learning češtiny, e-testy, lekce, kurzy, návody</title>

    <!-- Styles CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('partials._navbar')
    @include('partials._gotop')
    
    @yield('content')

    
    @include('partials._footer')

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    {{-- <script src="/js/main.js"></script> --}}
    <!-- Scripts CDN -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="/dist/sweetalert.min.js"></script>
    @include('sweet::alert')

    @yield('scripts')
</body>
</html>
