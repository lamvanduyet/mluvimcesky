@extends('layouts.app')

@section('title', trans('chapters.title'))

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- All Chapters -->
    <h1 class="section-title">{{trans('chapters.title')}}</h1>
	@if(count($chapters))

      @foreach($chapters as $chapter)
        @include('partials._chapter')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
      {{ $chapters->links() }}
      </div>
      
    @else
      <div class="col-sm-12 col-md-12">
        <p class="text-center">{{ trans('chapters.404') }}</p>
      </div>
    @endif	
    

  </div>
</div>
@endsection
