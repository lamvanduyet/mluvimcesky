@extends('layouts.app')

@section('title', $test->name)

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- Specific Lesson -->
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default post-head">
          <div class="panel-body">
            <h1 class="post-head__title">{{ $test->name }}</h1>
            <p class="post-head__date">{{ $test->created_at->format('d/m/y H:i') }}</p>
            <a href="{{ action('ChaptersController@chapter', $test->chapter->slug) }}"><p><i class="fa fa-files-o" aria-hidden="true"></i> {{$test->chapter->title}}</p></a>
            <hr>
            <p>{{$test->description}}</p>
            <ul class="list-inline post-head__bullets">
              <li class="post-head__time">{{ $test->estimated_time }} min</li>
              <li class="post-head__count">{{ count($test->questions) }} {{ trans_choice('tests.questions_count', count($test->questions)) }}</li>
              <li class="post-head__level">{{$test->difficulty}}</li>
              <li class="post-head__level">{{$test->is_paid ? trans('global.paid') : trans('global.free') }}</li>
            </ul>
            @if(count($test->questions))
            <div>
              <a class="btn post-head__start" href="{{ action('QuestionsController@index', $test->slug)}}">{{ trans('tests.start')}} <span class="glyphicon glyphicon-play"></span> </a>
            </div>
            @endif
          </div>
        </div>

    </div>

  </div>
</div>
@endsection
