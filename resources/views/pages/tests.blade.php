@extends('layouts.app')

@section('title', trans('tests.title') )

@section('content')
<!-- Main Content -->
<div class="container main-content">
  <div class="row">
    <h1 class="section-title">{{ trans('tests.title')}}</h1>

    @if(count($tests))
      @foreach($tests as $test)
        @include('partials._test')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
      {{ $tests->links() }}
      </div>

    @else
      <div class="col-sm-12 col-md-12">
      <p class="text-center">{{ trans('tests.404') }}</p>
      </div>
    @endif

    </div>
</div>
@endsection
