@extends('layouts.app')

@section('title', trans('results.title'))

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- Show test results -->
    <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default post-body">
          <div class="panel-body">

          <div class="alert alert-info">
            <h3>{{trans('results.title')}}: {{ round(($rightAnswersCount/$numOfQuestions)*100, 2) }}%</h3>
            <p>{{$rightAnswersCount}}/{{ $numOfQuestions }} {{ trans_choice('results.questions', $numOfQuestions)}}</p>
          </div>
          <hr>
          <?php $i=1; ?>
          @foreach($test->questions as $question)
              
                <h4>{{ $i }}. {{$question->question}}</h4>
                <?php $i++; ?>
              
                @foreach($question->choices as $choice)
                  <p class="{{ in_array($choice->id, $userAnswers) ? 'text-bold' : '' }} {{ in_array($choice->id, $correctChoices) ? ' text-success' : '' }}">{{$choice->choice}}</p>
                @endforeach

          @endforeach
          <hr>
            <a class="btn post-head__again" href="{{ action('QuestionsController@index', $test->slug)}}">{{ trans('tests.again')}} <span class="glyphicon glyphicon-refresh"></span></a>
            </div>
          </div>
        </div>

    </div>

  </div>
</div>
@endsection
