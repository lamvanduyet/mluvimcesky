@extends('layouts.app')

@section('title', trans('search.title'))

@section('content')
<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    <h1 class="section-title">{{ trans('search.results_for') }} "{{ $searchText}}"</h1>
    <div class="algolia"><span>powered by </span><a href="https://www.algolia.com/"><img class="img img-responsive" src="{{ asset('images/algolia.svg') }}" alt="algolia"></a></div>
    <!-- Found chapters -->
    <hr>
    <h2 class="section-title">{{ count($chapters) ? count($chapters) : '' }} {{ trans_choice('search.chapters_found', count($chapters)) }}</h2>

    @if(count($chapters))

      @foreach($chapters as $chapter)
        @include('partials._chapter')
      @endforeach

      {{-- <div class="col-sm-12 col-md-12 text-center">
      {{ $chapters->links() }}
      </div> --}}
      
    @endif

</div>
  <!-- Found lessons -->
<hr>

<div class="row">
<h2 class="section-title">{{ count($lessons) ? count($lessons) : '' }} {{ trans_choice('search.lessons_found', count($lessons)) }}</h2>
  @if(count($lessons))

      @foreach($lessons as $lesson)
        @include('partials._lesson')
      @endforeach

      {{-- <div class="col-sm-12 col-md-12 text-center">
      {{ $lessons->links() }}
      </div> --}}
      
    @endif

</div>

    <!-- Found tests -->
<hr>

<div class="row">
  <h2 class="section-title">{{ count($tests) ? count($tests) : '' }} {{ trans_choice('search.tests_found', count($tests)) }}</h2>

  @if(count($tests))
    @foreach($tests as $test)
      @include('partials._test')
    @endforeach

    {{-- <div class="col-sm-12 col-md-12 text-center">
    {{ $tests->links() }}
    </div> --}}

  @endif

</div>
</div>
@endsection
