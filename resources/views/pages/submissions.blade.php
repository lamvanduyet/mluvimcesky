@extends('layouts.app')

@section('title', trans('submissions.title'))

@section('content')
<div class="container main-content">
    <div class="row">
     <h1 class="section-title">{{ trans('submissions.title') }}</h1>
        <div class="col-md-8 col-md-offset-2">
            @if($submissions)
            <table class="table table-striped table-responsive">
                <thead>
                    <tr>
                        <th>{{trans('submissions.test_name')}}</th>
                        <th>{{trans('submissions.points')}}</th>
                        <th>{{trans('submissions.total_points')}}</th>
                        <th>{{trans('submissions.results')}}</th>
                        <th>{{trans('submissions.date')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($submissions as $submission)
                    <tr>
                        <td>{{$submission->test->name}}</td>
                        <td>{{$submission->answers->sum('is_right')}}</td>
                        <td>{{count($submission->test->questions)}}</td>
                        <td>{{ round(($submission->answers->sum('is_right')/count($submission->test->questions))*100, 2) }}%</td>
                        <td>{{ $submission->created_at->format('d/m/y H:i')}}</td>
                    </tr>
                @endforeach    
                </tbody>
            </table>
            {{ $submissions->links()}}
            @endif


        </div>
    </div>
</div>
@endsection