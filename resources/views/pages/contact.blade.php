@extends('layouts.app')

@section('title', trans('contact.title'))

@section('content')
<div class="container main-content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default card">
              <div class="panel-heading card-heading"><h1>{{ trans('contact.title') }}</h1></div>
              <div class="panel-body">
                <p class="text-center">{{ trans('contact.text') }}</p>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
