@extends('layouts.app')

@section('title', trans('lessons.title'))

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- All lessons -->
    <h1 class="section-title">{{trans('lessons.title')}}</h1>

    @if(count($lessons))

      @foreach($lessons as $lesson)
        @include('partials._lesson')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
      {{ $lessons->links() }}
      </div>
      
    @else
      <div class="col-sm-12 col-md-12">
        <p class="text-center">{{ trans('lessons.404') }}</p>
      </div>
    @endif

  </div>
</div>
@endsection
