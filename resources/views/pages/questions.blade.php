@extends('layouts.app')

@section('title', $test->name)

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- Specific Lesson -->
    <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default post-head">
          <div class="panel-body">
            <h1 class="post-head__title">{{$test->name}}</h1>
            <p class="post-head__date">{{$test->created_at->format('d/m/y H:i')}}</p>
            <a href="{{ action('ChaptersController@chapter', $test->chapter->slug) }}"><p><i class="fa fa-files-o" aria-hidden="true"></i> {{$test->chapter->title}}</p></a>
            <hr>
            <p>{{ $test->description }}</p>
            <ul class="list-inline post-head__bullets">
              <li class="post-head__time">{{$test->estimated_time}} min</li>
              <li class="post-head__count">{{ count($test->questions) }} {{ trans_choice('tests.questions_count', count($test->questions)) }}</li>
              <li class="post-head__level">{{ $test->difficulty }}</li>
            </ul>
          </div>
        </div>

        <div class="panel panel-default post-body">
          <div class="panel-body">
          <div id="timer"><i class="fa fa-clock-o" aria-hidden="true"></i> <span></span></div>
          <form action="{{ action('QuestionsController@evaluate', [$test->slug]) }}" method="post" class="form-horizontal" id="test-questions">
              
              <?php $i=1; ?>
              @foreach($test->questions as $question)

              <div class="col-md-8">
                <h4>{{ $i }}. {{$question->question}}</h4>
                <?php $i++; ?>
              </div>
                @foreach($question->choices as $choice)
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-1">
                    <input type="radio" name="question_{{$question->id}}" id="question_{{$question->id}}" value="{{$choice->id}}">
                    <label for="question_{{$question->id}}">{{$choice->choice}}</label>
                  </div>
                </div>
                @endforeach

              @endforeach
                {!! csrf_field() !!}
                <button type="submit" class="btn post-body__end">{{ trans('tests.end') }}</button>
          </form>
            </div>
          </div>
        </div>

    </div>

  </div>
</div>

@section('scripts')

<script src="/js/jquery.countdown.min.js"></script>

<script>
var time = new Date().getTime() + {{$test->estimated_time*60000}};
  $("#timer>span")
  .countdown(time, function(event) {
    $(this).text(
      event.strftime('%H:%M:%S')
    );
  });
</script>

<script>
  function submit(){
  $("#test-questions").submit();
  }
  setTimeout(submit, {{$test->estimated_time * 60000}});
</script>

@endsection
