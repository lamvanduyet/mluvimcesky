@extends('layouts.app')

@section('title', $lesson->title)

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- Specific Lesson -->
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default post-head">
          <div class="panel-body">
            <h1 class="post-head__title">{{ $lesson->title }}</h1>
            <p class="post-head__date">{{ $lesson->created_at->format('d/m/Y H:i') }}</p>
            <a href="{{ action('ChaptersController@chapter', $lesson->chapter->slug) }}"><p><i class="fa fa-files-o" aria-hidden="true"></i> {{$lesson->chapter->title}}</p></a>
            <hr>
            <p>{{$lesson->description}}</p>
            <ul class="list-inline post-head__bullets">
              <li class="post-head__time">{{ $lesson->estimated_time }} min</li>
              <li class="post-head__level">{{ $lesson->difficulty }}</li>
              <li class="post-head__level">{{ $lesson->is_paid ? trans('global.paid') : trans('global.free') }}</li>
            </ul>
          </div>
        </div>
        
        <!--Video Section-->
        @if($lesson->video_url)
          <div class="panel panel-default post-body">
            <div class="panel-body">
            
            <div align="center" class="embed-responsive embed-responsive-16by9">
              <video controls class="embed-responsive-item">
                <source src="{{ Storage::disk('s3')->url($lesson->video_url) }}" type="video/mp4">
                Your browser does not support HTML5 video.
              </video>
            </div>

            </div>
          </div>
        @endif
        
        <!--Audio Section-->
        @if($lesson->audio_url)
          <div class="panel panel-default post-body">
            <div class="panel-body">
            
            <div align="center" class="embed-responsive embed-responsive-16by9">
              <audio controls class="embed-responsive-item">
                <source src="{{ Storage::disk('s3')->url($lesson->audio_url) }}" type="audio/mpeg">
                Your browser does not support the audio element.
              </audio>
            </div>

            </div>
          </div>
        @endif

        <div class="panel panel-default post-body">
          <div class="panel-body">
          @if (App::isLocale('cs'))
            {!! $lesson->content !!}
          @else
            {!! $lesson->content_vn !!}  
          @endif  
          </div>
        </div>

    <div id="disqus_thread"></div>

    </div>

  </div>
</div>
@endsection
