@extends('layouts.app')

@section('title', trans('home.title'))

@section('content')
<!-- Jumbotron -->
<div class="jumbotron">
  <div class="container">
    <h1>MluvímČesky.cz</h1>
  </div>
</div>

<!-- Main Content -->
<div class="container main-content">

<div class="row">
    
    <!-- Featured chapters -->
    <h2 class="section-title">{{ trans('chapters.featured') }}</h2>

    @if(count($chapters))

      @foreach($chapters as $chapter)
        @include('partials._chapter')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
        <a class="btn btn-custom" href="{{ url('chapters')}}">{{ trans('buttons.more') }}</a>
    </div>
      
    @else
      <div class="col-sm-12 col-md-12">
        <p class="text-center">{{ trans('chapters.404') }}</p>
      </div>
    @endif

  </div>

  <hr>

  <!-- Featured lessons -->
  <div class="row">
    
    
    <h2 class="section-title">{{ trans('lessons.featured') }}</h2>

    @if(count($lessons))

      @foreach($lessons as $lesson)
        @include('partials._lesson')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
        <a class="btn btn-custom" href="{{ url('lessons')}}">{{ trans('buttons.more') }}</a>
    </div>
      
    @else
      <div class="col-sm-12 col-md-12">
        <p class="text-center">{{ trans('lessons.404') }}</p>
      </div>
    @endif

  </div>
    
    <hr>
  <!-- Featured tests -->
  <div class="row">
    <h2 class="section-title">{{ trans('tests.featured') }}</h2>

    @if(count($tests))
      @foreach($tests as $test)
        @include('partials._test')
      @endforeach

      <div class="col-sm-12 col-md-12 text-center">
        <a class="btn btn-custom" href="{{ url('tests') }}">{{ trans('buttons.more') }}</a>
    </div>

    @else
      <div class="col-sm-12 col-md-12">
      <p class="text-center">{{ trans('tests.404') }}</p>
      </div>
    @endif

    </div>
</div>
@endsection
