@extends('layouts.app')

@section('title', $chapter->title)

@section('content')

<!-- Main Content -->
<div class="container main-content">
    <div class="row">
    
    <!-- Specific Chapter -->
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default post-head">
          <div class="panel-body">
            <h1 class="post-head__title">{{ $chapter->title }}</h1>
            <p class="post-head__date">{{ $chapter->created_at->format('d/m/y H:i') }}</p>
            <hr>
            <p>{{$chapter->description}}</p>
            <ul class="list-inline post-head__bullets">
              <li class="post-head__time">{{ $chapter->estimated_time }} min</li>
              <li class="post-head__level">{{$chapter->difficulty}}</li>
              <li class="post-head__level">{{$chapter->is_paid ? trans('global.paid') : trans('global.free') }}</li>
            </ul>
          </div>
        </div>

        @if(count($chapter->lessons) or count($chapter->tests) )

        <div class="panel panel-default">
          <!-- List group -->
          <div class="panel-body"><h2 class="text-center">{{trans('lessons.title')}} &amp; {{ trans('tests.title')}}</h2></div>
          
            <ul class="list-group chapter-list">
              <?php $i=1; ?>
              @foreach($chapter->lessons as $lesson)
                <a href="{{ action('LessonsController@lesson', $lesson->slug) }}" class="list-group-item"><li><span class="chapter-list-order">{{$i}}.</span> <i title="{{trans('lessons.title')}}" class="fa fa-book" aria-hidden="true"></i> {{$lesson->title}} <span class="label {{ $lesson->is_paid ? 'label-danger' : 'label-success' }}">{{ $lesson->is_paid ? trans('global.paid') : trans('global.free') }}</span> <div class="pull-right">{{$lesson->estimated_time}} min</div></li></a>
                <?php $i++; ?>
              @endforeach 
            
              @foreach($chapter->tests as $test)
                <a href="{{ action('TestsController@test', $test->slug) }}" class="list-group-item"><li><span class="chapter-list-order">{{$i}}.</span> <i title="{{trans('tests.title')}}" class="fa fa-check-square-o" aria-hidden="true"></i> {{$test->name}} <span class="label {{ $test->is_paid ? 'label-danger' : 'label-success' }}">{{ $test->is_paid ? trans('global.paid') : trans('global.free') }}</span> <div class="pull-right">{{$test->estimated_time}} min</div></li></a>
                <?php $i++; ?>
               @endforeach 
              
            </ul>
        </div>
        @else
        <div class="panel panel-default">
          <div class="panel-body text-center">Empty</div>
        </div>
        @endif

    </div>

  </div>
</div>
@endsection
