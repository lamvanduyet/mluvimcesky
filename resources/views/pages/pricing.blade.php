@extends('layouts.app')

@section('title', trans('pricing.title'))

@section('content')
<div class="container main-content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default card">
              <div class="panel-heading card-heading"><h1>{{ trans('pricing.title') }}</h1></div>
              <div class="panel-body">
                <h3 class="text-center">{{ trans('pricing.price') }}</h3>
                <p class="text-center">{{ trans('pricing.text') }}</p>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
