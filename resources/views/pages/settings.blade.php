@extends('layouts.app')

@section('title', trans('settings.title'))

@section('content')
<div class="container main-content">
    <div class="row">
        <!-- TABS -->
        <div class="col-md-4">
            <ul class="nav nav-pills nav-stacked settings-nav">

                <li role="presentation" class="active">
                    <a href="#profile-settings" class="profile-settings" aria-controls="profile-settings" role="tab" data-toggle="tab">{{ trans('settings.personal_settings') }}
                    </a>
                </li>

                <li role="presentation">
                    <a href="#password-settings" class="password-settings" aria-controls="password-settings" role="tab" data-toggle="tab">{{ trans('settings.password_settings') }}
                    </a>
                </li>

                <li role="presentation">
                    <a href="#email-settings" class="email-settings" aria-controls="email-settings" role="tab" data-toggle="tab">{{ trans('settings.email_settings') }}
                    </a>
                </li>

                <li role="presentation">
                    <a href="#username-settings" class="username-settings" aria-controls="username-settings" role="tab" data-toggle="tab">
                    {{ trans('settings.username_settings') }}
                    </a>
                </li>

                <li role="presentation">
                    <a href="#payment-settings" class="payment-settings" aria-controls="payment-settings" role="tab" data-toggle="tab">
                    {{ trans('settings.payment') }}
                    </a>
                </li>

            </ul>    
        </div>

        <!-- TABS Content --> 
        <div class="col-md-8">
            <div class="tab-content settings-content">
                
                <!-- Personal Settings-->
                <div role="tabpanel" class="tab-pane active" id="profile-settings">
                    <div class="profile-settings">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="panel panel-default">

                                    <form class="form-horizontal row-border" method="post" action="{{ url('settings/avatar/update')}}" enctype="multipart/form-data">
                                        <div class="panel-body">
                                            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                                                <div class="col-sm-6">
                                                    <div class="user-avatar img-circle">
                                                        {{-- <img class="img-responsive" src="{{ url(asset($user->avatar)) }}" alt="avatar"> --}}
                                                        <img class="img-responsive" src="{{ Storage::disk('s3')->url($user->avatar) }}" alt="avatar">
                                                    </div>

                                                    <div class="animate-btn">
                                                        <h3 class="panel-title">{{ trans('settings.upload_avatar') }}</h3>
                                                        
                                                        <input type="file" name="avatar" id="avatar" data-maxsize="30720" accept="image/jpg, image/png, image/jpeg" onchange='this.form.submit()'>
                                                    </div>
                                                    @if ($errors->has('avatar'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('avatar') }}</strong>
                                                    </span>
                                                    @endif
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <noscript><input type="submit" value="Submit"></noscript>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="panel-title">{{ trans('settings.personal_settings') }}</h3>
                                        
                                        {!! Form::model($user,['method' => 'POST','url' => 'settings/personal/update']) !!}
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                {!! Form::label('name', trans('settings.name'), ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::text('name',null,['class' => 'form-control']) !!}
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                {!! Form::submit(trans('buttons.update'),['class' => 'btn btn-custom']) !!}
                                                </div>
                                            </div>
                                        {!! Form::close() !!}

                                    </div> 
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Change Password-->
                <div role="tabpanel" class="tab-pane" id="password-settings">
                    <div class="password-settings">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="panel-title">{{ trans('settings.password_settings') }}</h3>

                                        {!! Form::open(['method' => 'POST','url' => 'settings/password/update']) !!}

                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                {!! Form::label('password', trans('settings.old_pw'), ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::password('password',['class' => 'form-control']) !!}
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                                {!! Form::label('new_password', trans('settings.new_pw'), ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::password('new_password',['class' => 'form-control']) !!}
                                                @if ($errors->has('new_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                                {!! Form::label('new_password_confirmation', trans('settings.confirm_new'), ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::password('new_password_confirmation',['class' => 'form-control']) !!}
                                                @if ($errors->has('new_password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                {!! Form::submit(trans('buttons.update'),['class' => 'btn btn-custom']) !!}
                                                </div>
                                            </div>
                                        {!! Form::close() !!}


                                    </div> 
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Change email-->
                <div role="tabpanel" class="tab-pane" id="email-settings">
                    <div class="email-settings">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="panel-title">{{ trans('settings.email_settings') }}</h3>

                                        {!! Form::model($user, ['method' => 'post', 'url' => 'settings/email/update']) !!}
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                {!! Form::label('email', 'E-Mail', ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::email('email',null,['class' => 'form-control']) !!}
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                {!! Form::submit(trans('buttons.update'),['class' => 'btn btn-custom']) !!}
                                                </div>
                                            </div>
                                        {!! Form::close() !!}

                                    </div> 
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Change username -->
                <div role="tabpanel" class="tab-pane" id="username-settings">
                    <div class="username-settings">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="panel-title">{{ trans('settings.username_settings') }}</h3>    

                                        {!! Form::model($user, ['method' => 'post', 'url' => 'settings/username/update']) !!}

                                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                                {!! Form::label('username', trans('settings.username'), ['class' => 'col-md-4', 'control-label']) !!}
                                                <div class="col-md-6">
                                                {!! Form::text('username',null,['class' => 'form-control']) !!}

                                                @if ($errors->has('username'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                {!! Form::submit(trans('buttons.update'),['class' => 'btn btn-custom']) !!}
                                                </div>
                                            </div>
                                        {!! Form::close() !!}

                                    </div> 
                                </div>
                                

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Payment -->
                <div role="tabpanel" class="tab-pane" id="payment-settings">
                    <div class="payment-settings">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h3 class="panel-title">{{ trans('settings.payment') }}</h3>
                                        
                                        @if(!auth()->user()->subscribed('main'))
                                        <form action="{{ action('UsersController@payment') }}" method="POST" id="payment-form">
                                          <span class="payment-errors"></span>

                                          <div class="form-group">
                                            <label for="cardNumber" class="col-md-4 control-label">{{ trans('settings.card_number') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" size="20" data-stripe="number" id="cardNumber" class="form-control cardNumber" placeholder="XXXX YYYY XXXX YYYY">   
                                            </div>
                                            <div class="col-xs-2 p0">
                                                <img src="/img/credit-cards/credit.png" width="50" height="34" class="credit-card plus">
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="expMonth" class="col-md-4 control-label">{{ trans('settings.expiration') }}</label>
                                            <div class="col-md-6">
                                                <input type="text" size="2" data-stripe="exp_month" class="form-control" id="exp-month" placeholder="MM">
                                                <input type="text" size="2" data-stripe="exp_year" class="form-control" id="exp-year" placeholder="YY">
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="cvc" class="col-md-4 control-label">CVC</label>
                                            <div class="col-md-6">
                                              <input type="text" size="4" data-stripe="cvc" class="form-control" id="cvc" placeholder="CVC">
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="zip" class="col-md-4 control-label">{{ trans('settings.zip') }}</label>
                                            <div class="col-md-6">
                                              <input type="text" size="6" data-stripe="address_zip" class="form-control" id="zip">
                                            </div>
                                          </div>

                                            <input type="hidden" data-stripe="name" value="{{auth()->user()->email}}" hidden>
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-custom">{{ trans('settings.subscribe') }}</button>
                                                </div>
                                            </div>

                                        </form>
                                        <!--If User has cancelled the subscription he might be on grace period or not-->
                                        @elseif(auth()->user()->subscription('main')->cancelled())
                                            @if(auth()->user()->subscription('main')->onGracePeriod())
                                            <p>{{ trans('settings.grace_period') }}</p>
                                            @else
                                                <p>{{ trans('settings.cancelled') }}</p>
                                            @endif

                                        <hr>
                                            <table class="table table-striped responsive">
                                                @foreach (auth()->user()->invoicesIncludingPending() as $invoice)
                                                <thead>
                                                    <tr>
                                                        <th>{{ trans('settings.from')}}</th>
                                                        <th>{{ trans('settings.total')}}</th>
                                                        <th>{{ trans('settings.download')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                                        <td>{{ $invoice->total() }}</td>
                                                        <td><a href="/user/invoice/{{ $invoice->id }}">{{ trans('settings.download')}}</a></td>
                                                    </tr>
                                                </tbody>
                                                    
                                                @endforeach
                                            </table>
                                        <hr>
                                        
                                        <form action="{{ action('UsersController@resumePayment')}}" method="POST">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-success">{{ trans('settings.resume')}}</button>
                                        </form>

                                        <!--User is subscribed-->
                                        @else
                                        <p>{{ trans('settings.subscribed') }}</p>
                                        <hr>
                                        <table class="table table-striped responsive">
                                            @foreach (auth()->user()->invoicesIncludingPending() as $invoice)
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('settings.from')}}</th>
                                                    <th>{{ trans('settings.total')}}</th>
                                                    <th>{{ trans('settings.download')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                                    <td>{{ $invoice->total() }}</td>
                                                    <td><a href="/user/invoice/{{ $invoice->id }}">{{ trans('settings.download')}}</a></td>
                                                </tr>
                                            </tbody>
                                                
                                            @endforeach
                                        </table>
                                        <hr>
                                        <form action="{{ action('UsersController@cancelPayment')}}" method="POST">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger">{{ trans('settings.cancel')}}</button>
                                        </form>
                                        @endif

                                    </div> 
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

        </div>


    </div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    // This identifies your website in the createToken call below
    Stripe.setPublishableKey('pk_test_g9RhNwaAOKbhhPOczIvBXJtM');
</script>
@endsection
