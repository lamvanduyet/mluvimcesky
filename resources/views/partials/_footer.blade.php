	<!-- Footer Top -->
	<footer class="footer-top">
      <div class="container">
      <div class="col-md-6 col-sm-6">
      	<h3>MluvímČesky</h3>
      	<p>{{ trans('footer.mluvimcesky') }}</p>
      	<form action="{{action('PagesController@newsletter')}}" method="post" class="form-inline">
	      	<div class="form-group">
	      		<input type="text" name="news_email" class="form-control" placeholder="{{ trans('footer.newsletter') }}">
            {!! csrf_field() !!}
	      		<button class="btn btn-custom" type="submit">{{ trans('footer.subscribe') }}</button>	
	      	</div>
      	</form>
      </div>
      <div class="col-md-3 col-sm-3">
      	<h4>{{ trans('footer.navigation') }}</h4>
      	<ul>
      		<li><a class="text-muted" href="{{ url('/') }}">{{ trans('footer.home') }}</a></li>
      		<li><a class="text-muted" href="{{ url('/lessons') }}">{{ trans('footer.lessons') }}</a></li>
      		<li><a class="text-muted" href="{{ url('/etests') }}">{{ trans('footer.tests') }}</a></li>
      		<li><a class="text-muted" href="{{ url('/pricing')}}">{{ trans('footer.pricing') }}</a></li>
      		<li><a class="text-muted" href="{{ url('/contact')}}">{{ trans('footer.contact') }}</a></li>
      	</ul>
      </div>
      <div class="col-md-3 col-sm-3">
      	<h4>{{ trans('footer.auth') }}</h4>
      	<ul>
      		<li><a class="text-muted" href="{{ url('/login') }}">{{ trans('footer.login') }}</a></li>
      		<li><a class="text-muted" href="{{ url('/register') }}">{{ trans('footer.register') }}</a></li>
      	</ul>
      </div>
      </div>
    </footer>
    <!-- Footer Bottom -->
	<footer class="footer-bottom text-center">
      <div class="container">
        <p class="text-white copyright">&copy;Copyright 2016 - MluvímČesky.cz</p>
        <form action="{{url('lang')}}" method="get">
        	<select name="lang" id="lang" onchange='this.form.submit()' class="form-control lang-chooser">
            <option value="cs" {{ App::isLocale('cs') ? 'selected=selected' : '' }}>Čeština</option>
        		<option value="vi" {{ App::isLocale('vi') ? 'selected=selected' : '' }}>Tiếng Việt</option>
        	</select>
          {!! csrf_field() !!}
          <noscript><input type="submit" value="Submit"></noscript>
        </form>
        <ul class="list-inline social-icons">
        	<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        	<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        	<li><a href=""><i class="fa fa-google-plus-official" aria-hidden="true"></i></a></li>
        	<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </footer>