      <div class="col-sm-6 col-md-4">
          <div class="panel panel-default card">
            <div class="panel-heading card-heading"><span class="pull-left"><i title="{{trans('lessons.title')}}" class="fa fa-book" aria-hidden="true"></i></span>{{ $lesson->difficulty }}</div>
            <div class="panel-body text-center card-body">
              <h3><a href="{{ action('LessonsController@lesson', [$lesson->slug])}}">{{ $lesson->title }}</a></h3>
              <p>{{ $lesson->description }}</p>
            </div>
            <div class="panel-footer card-footer">
             <div class="pull-left">{{ trans('global.estimated_time') }}: <b>{{ $lesson->estimated_time }}</b> min</div>
             <div class="pull-right {{ $lesson->is_paid ? 'text-danger' : 'text-success' }}">{{ $lesson->is_paid ? trans('global.paid') : trans('global.free') }}</div>  
             <div class="clearfix"></div> 
            </div>
          </div>
      </div>