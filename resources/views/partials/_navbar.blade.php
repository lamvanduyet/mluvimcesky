    <nav class="navbar navbar-default navbar-fixed-top navigation">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand logo" href="{{ url('/') }}">
                    {{ config('app.name', 'MluvímČesky') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav main-menu">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('navbar.learning') }} <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{ url('/chapters') }}">{{ trans('navbar.chapters') }}</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/lessons') }}">{{ trans('navbar.lessons') }}</a></li>
                        <li><a href="{{ url('/etests') }}">{{ trans('navbar.tests') }}</a></li>
                      </ul>
                    </li>
                    <li><a href="{{ url('/pricing')}}">{{ trans('navbar.pricing') }}</a></li>
                    <li><a href="{{ url('/contact')}}">{{ trans('navbar.contact') }}</a></li>
                </ul>

              {{-- <form class="navbar-form navbar-right navbar-search">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
              </form> --}}

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right authentication-nav">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a class="btn-register" href="{{ url('/register') }}">{{ trans('navbar.join') }}</a></li>
                        <li><a href="{{ url('/login') }}">{{ trans('navbar.login') }}</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{-- <div class="avatar"><img src="{{ asset(Auth::user()->avatar) }}" class="img-responsive"> --}}
                                <div class="avatar"><img src="{{ Storage::disk('s3')->url(auth()->user()->avatar) }}" class="img-responsive">
                                </div>  {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/submissions') }}">
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i> {{ trans('navbar.submissions') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/settings') }}">
                                        <i class="fa fa-cog" aria-hidden="true"></i> {{ trans('navbar.settings') }}
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out" aria-hidden="true"></i> {{ trans('navbar.logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                        <li>
                            <form class="navbar-form navbar-search" method="GET" action="{{ url('search') }}">
                                <div class="form-group">
                                  <input type="text" class="form-control" name="search" id="search" placeholder="{{ trans('navbar.search') }}">
                                </div>
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-default search-button"><span class="glyphicon glyphicon-search"></span></button>
                            </form>
                        </li>
                </ul>
            </div>
        </div>
    </nav>