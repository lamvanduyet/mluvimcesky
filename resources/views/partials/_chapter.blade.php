		<div class="col-sm-6 col-md-4">
		  <div class="panel panel-default card-chapter">
		    <div class="panel-heading card-chapter-heading"><span class="pull-left"><i title="{{trans('chapters.title')}}" class="fa fa-files-o" aria-hidden="true"></i></span>{{ $chapter->difficulty }}</div>
		    <div class="panel-body text-center card-body">
		      <h3><a href="{{ action('ChaptersController@chapter', [$chapter->slug])}}">{{ $chapter->title }}</a></h3>
		      <p>{{ $chapter->description }}</p>
		    </div>
		    <div class="panel-footer card-footer">
		     <div class="pull-left">{{ trans('global.estimated_time') }}: <b>{{ $chapter->estimated_time }}</b> min</div>
		     <div class="pull-right {{ $chapter->is_paid ? 'text-danger' : 'text-success' }}">{{ $chapter->is_paid ? trans('global.paid') : trans('global.free') }}</div>  
		     <div class="clearfix"></div> 
		    </div>
		  </div>
		</div>