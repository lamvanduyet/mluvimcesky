      <div class="col-sm-6 col-md-4">
          <div class="panel panel-default card">
            <div class="panel-heading card-heading"><span class="pull-left"><i title="{{trans('tests.title')}}" class="fa fa-check-square-o" aria-hidden="true"></i></span>{{$test->difficulty}}</div>
            <div class="panel-body text-center card-body">
              <h3><a href="{{ action('TestsController@test', [$test->slug])}}">{{ $test->name }}</a></h3>
              <p>{{ $test->description }}</p>
            </div>
            <div class="panel-footer card-footer">
             <div class="pull-left">{{ trans('global.estimated_time') }}: <b>{{ $test->estimated_time }}</b> min</div>
             <div class="pull-right {{ $test->is_paid ? 'text-danger' : 'text-success' }}">{{$test->is_paid ? trans('global.paid') : trans('global.free') }}</div>  
             <div class="clearfix"></div> 
            </div>
          </div>
      </div>