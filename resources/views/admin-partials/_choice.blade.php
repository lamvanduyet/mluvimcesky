        <!--Choice-->
        <div class="form-group{{ $errors->has('choice') ? ' has-error' : '' }}">
            {!! Form::label('choice', 'Choice', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::text('choice',null,['class' => 'form-control']) !!}
            @if ($errors->has('choice'))
                <span class="help-block">
                    <strong>{{ $errors->first('choice') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Choice-->


         <!--Type-->
        <div class="form-group{{ $errors->has('is_right_choice') ? ' has-error' : '' }}">
            {!! Form::label('is_right_choice', 'Right Choice?', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::select('is_right_choice',['0' => 'No', '1' => 'Yes'],null,['class' => 'form-control']) !!}
            @if ($errors->has('is_right_choice'))
                <span class="help-block">
                    <strong>{{ $errors->first('is_right_choice') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Type-->

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
            {!! Form::submit($submitText,['class' => 'btn btn-custom']) !!}
            </div>
        </div>