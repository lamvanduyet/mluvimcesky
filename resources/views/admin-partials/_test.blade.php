<!--Chapter-->
        <div class="form-group{{ $errors->has('chapter_id') ? ' has-error' : '' }}">
            {!! Form::label('chapter_id', 'Chapter', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::select('chapter_id',$chapters,null,['class' => 'form-control']) !!}
            @if ($errors->has('chapter_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('chapter_id') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Chapter-->

<!--Name-->
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Name', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::text('name',null,['class' => 'form-control']) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Name-->

        <!--Slug-->
        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
            {!! Form::label('slug', 'Slug', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::text('slug',null,['class' => 'form-control']) !!}
            @if ($errors->has('slug'))
                <span class="help-block">
                    <strong>{{ $errors->first('slug') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Slug-->

        <!--Description-->
        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {!! Form::label('description', 'Short Description', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::textarea('description',null,['class' => 'form-control']) !!}
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Description-->

        <!--Difficulty-->
        <div class="form-group{{ $errors->has('difficulty') ? ' has-error' : '' }}">
            {!! Form::label('difficulty', 'Difficulty', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::select('difficulty',['beginner' => 'Beginner', 'intermidiate' => 'Intermidiate', 'advanced' => 'Advanced'],null,['class' => 'form-control']) !!}
            @if ($errors->has('difficulty'))
                <span class="help-block">
                    <strong>{{ $errors->first('difficulty') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Difficulty-->

        <!--Is Paid-->
        <div class="form-group{{ $errors->has('is_paid') ? ' has-error' : '' }}">
            {!! Form::label('is_paid', 'Is Paid', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::select('is_paid',['0' => 'No', '1' => 'Yes'],null,['class' => 'form-control']) !!}
            @if ($errors->has('is_paid'))
                <span class="help-block">
                    <strong>{{ $errors->first('is_paid') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Is Paid-->

        <!--Estimated Time-->
        <div class="form-group{{ $errors->has('estimated_time') ? ' has-error' : '' }}">
            {!! Form::label('estimated_time', 'Estimated Time (min)', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::text('estimated_time',null,['class' => 'form-control']) !!}
            @if ($errors->has('estimated_time'))
                <span class="help-block">
                    <strong>{{ $errors->first('estimated_time') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Estimated Time-->

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
            {!! Form::submit($submitText,['class' => 'btn btn-custom']) !!}
            </div>
        </div>