
{{--          <!--Type-->
        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
            {!! Form::label('type', 'Type', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::select('type',['choose' => 'Choose', 'insert' => 'Insert'],null,['class' => 'form-control']) !!}
            @if ($errors->has('type'))
                <span class="help-block">
                    <strong>{{ $errors->first('type') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Type--> --}}

        <!--Question-->
        <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
            {!! Form::label('question', 'Question', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::text('question',null,['class' => 'form-control']) !!}
            @if ($errors->has('question'))
                <span class="help-block">
                    <strong>{{ $errors->first('question') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Question-->

		<!--Audio-->
        <div class="form-group{{ $errors->has('audio') ? ' has-error' : '' }}">
            {!! Form::label('audio', 'Audio', ['class' => 'col-md-4', 'control-label']) !!}
            <div class="col-md-6">
            {!! Form::file('audio',null,['class' => 'form-control']) !!}
            @if ($errors->has('audio'))
                <span class="help-block">
                    <strong>{{ $errors->first('audio') }}</strong>
                </span>
                @endif
            </div>
        </div><!-- End Audio-->

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
            {!! Form::submit($submitText,['class' => 'btn btn-custom']) !!}
            </div>
        </div>