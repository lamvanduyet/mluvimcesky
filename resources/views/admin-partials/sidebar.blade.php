      <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="user">
              <div class="user-avatar img-circle">
                <img class="img-responsive" src="{{ Storage::disk('s3')->url(auth()->user()->avatar) }}" alt="avatar">
              </div>
              <p class="user-welcome">{{ auth()->user()->name}}</p>
            </li>
            <li {{ (Request::is('admin/dashboard')) ? 'class=active': '' }}><a href="{{ url('admin/dashboard')}}"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
            <li {{ (Request::is('admin/chapters')) ? 'class=active': '' }}><a href="{{ url('admin/chapters')}}"><span class="glyphicon glyphicon-folder-open"></span> Chapters</a></li>
            <li {{ (Request::is('admin/lessons')) ? 'class=active': '' }}><a href="{{ url('admin/lessons')}}"><span class="glyphicon glyphicon-book"></span> Lessons</a></li>
            <li {{ (Request::is('admin/tests')) ? 'class=active': '' }}><a href="{{ url('admin/tests')}}"><span class="glyphicon glyphicon-check"></span> Tests</a></li>
            <li {{ (Request::is('admin/users')) ? 'class=active': '' }}><a href="{{ url('admin/users')}}"><span class="glyphicon glyphicon-user"></span> Users</a></li>
          </ul>
          <hr>
          <ul class="nav nav-sidebar">
            <li {{ (Request::is('admin/add-chapter')) ? 'class=active': '' }}><a href="{{ url('admin/add-chapter')}}"><span class="glyphicon glyphicon-plus-sign"></span> Add Chapter</a></li>
            <li {{ (Request::is('admin/add-lesson')) ? 'class=active': '' }}><a href="{{ url('admin/add-lesson')}}"><span class="glyphicon glyphicon-plus-sign"></span> Add Lesson</a></li>
            <li {{ (Request::is('admin/add-test')) ? 'class=active': '' }}><a href="{{ url('admin/add-test')}}"><span class="glyphicon glyphicon-plus-sign"></span> Add Test</a></li>
            {{-- <li {{ (Request::is('admin/add-questions')) ? 'class=active': '' }}><a href="{{ url('admin/add-questions')}}">Add Questions</a></li> --}}
          </ul>
      </div>