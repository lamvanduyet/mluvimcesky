@extends('layouts.admin')

@section('title', 'Edit Test')

@section('content')
	<h1>Edit Test</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@tests')}}"><span aria-hidden="true">&larr;</span> Tests</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::model($test,['method' => 'POST', 'action' => ['AdminController@testUpdate', $test->id], 'files' => true]) !!}
        @include('admin-partials._test', ['submitText' => 'Update Test'])
    {!! Form::close() !!}
    </div>
	</div>
@endsection