
@extends('layouts.admin')

@section('title', 'Add Question')

@section('content')
	<h1>Add Question</h1>
	<hr>
	
	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@testQuestions', [$test->id])}}"><span aria-hidden="true">&larr;</span> {{$test->name}}</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::open(['method' => 'POST','action' => ['AdminController@questionCreate', $test->id], 'files' => true]) !!}
		@include('admin-partials._question', ['submitText' => 'Add Question'])
    {!! Form::close() !!}
    </div>
	</div>
@endsection