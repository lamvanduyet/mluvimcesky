@extends('layouts.admin')

@section('title', 'Edit Lesson')

@section('content')
	<h1>Edit Lesson</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@lessons')}}"><span aria-hidden="true">&larr;</span> Lessons</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::model($lesson,['method' => 'POST', 'action' => ['AdminController@lessonUpdate', $lesson->id], 'files' => true]) !!}
        @include('admin-partials._lesson', ['submitText' => 'Update Lesson'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection