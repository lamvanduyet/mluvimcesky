@extends('layouts.admin')

@section('title', 'Add Chapter')

@section('content')
	<h1>Add Chapter</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@chapters')}}"><span aria-hidden="true">&larr;</span> Chapters</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::open(['method' => 'POST','url' => 'admin/chapters/create', 'files' => true]) !!}
        @include('admin-partials._chapter', ['submitText' => 'Add Chapter'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection