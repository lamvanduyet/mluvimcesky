@extends('layouts.admin')

@section('title', 'Choices')

@section('content')
	<h1>Choices for question: <small>{{$question->question}}</small></h1>
	<hr>

    <div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@testQuestions', [$question->test->id])}}"><span aria-hidden="true">&larr;</span> Questions</a>
            </li>
            <li>
                <a href="{{action('AdminController@addChoice', [$question->id])}}" class="btn btn-sm btn-info">Add choices</a>
            </li>
        </ul>   
    </div>


	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Question No.</th>
                <th>Choice</th>
                <th>Is Right Choice</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Question No.</th>
                <th>Choice</th>
                <th>Is Right Choice</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($question->choices))
            @foreach($question->choices as $choice)
                <tr>
                    <td>{{ $choice->id }}</td>
                    <td>{{ $choice->question_id }}</td>
                    <td>{{ $choice->choice }}</td>
                    <td>{{ $choice->is_right_choice ? 'Yes' : 'No'}}</td>
                    <td>{{ $choice->created_at->format('d/m/y H:i') }}</td>
                    <td>{{ $choice->updated_at->format('d/m/y H:i') }}</td>
                    <td><a href="{{ action('AdminController@editChoice', [$choice->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                    <td>
                        <form action="{{ action('AdminController@choiceDelete')}}" method="post" class="form-delete">
                            <input type="hidden" name="choice" value="{{$choice->id}}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-xs btn-danger delete">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif    
        </tbody>
    </table>


@endsection