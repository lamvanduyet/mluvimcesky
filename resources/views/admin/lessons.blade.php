@extends('layouts.admin')

@section('title', 'Lessons')

@section('content')
	<h1>Lessons</h1>
	<hr>

	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Chapter</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Chapter</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($lessons))
            @foreach($lessons as $lesson)
                <tr>
                    <td>{{ $lesson->id }}</td>
                    <td>{{$lesson->chapter->title}}</td>
                    <td>{{ $lesson->title }}</td>
                    <td>{{ $lesson->slug }}</td>
                    <td>{{ $lesson->description}}</td>
                    <td>{{ $lesson->difficulty}}</td>
                    <td>{{ $lesson->is_paid ? 'Paid' : 'Free'}}</td>
                    <td>{{ $lesson->estimated_time}} </td>
                    <td>{{ $lesson->created_at->format('d/m/y H:i') }}</td>
                    <td>{{ $lesson->updated_at->format('d/m/y H:i') }}</td>
                    <td><a href="{{ action('LessonsController@lesson', [$lesson->slug])}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a></td>
                    <td><a href="{{ action('AdminController@editLesson',[$lesson->id])}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                    <td>
                        <form action="{{ action('AdminController@lessonDelete')}}" method="post" class="form-delete">
                            <input type="hidden" name="lesson" value="{{$lesson->id}}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-xs btn-danger delete">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif 
        </tbody>
    </table>
@endsection