@extends('layouts.admin')

@section('title', 'Edit Chapter')

@section('content')
	<h1>Edit Chapter</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@chapters')}}"><span aria-hidden="true">&larr;</span> Chapters</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::model($chapter,['method' => 'POST', 'action' => ['AdminController@chapterUpdate', $chapter->id], 'files' => true]) !!}
        @include('admin-partials._chapter', ['submitText' => 'Update Chapter'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection