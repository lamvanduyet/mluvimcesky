
@extends('layouts.admin')

@section('title', 'Add Test')

@section('content')
	<h1>Add Test</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@tests')}}"><span aria-hidden="true">&larr;</span> Tests</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::open(['method' => 'POST','url' => 'admin/tests/create', 'files' => true]) !!}
        @include('admin-partials._test', ['submitText' => 'Add Test'])
    {!! Form::close() !!}
    </div>
	</div>
@endsection