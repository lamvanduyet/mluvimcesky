@extends('layouts.admin')

@section('title', 'Chapters')

@section('content')
	<h1>Chapters</h1>
	<hr>

	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($chapters))
            @foreach($chapters as $chapter)
                <tr>
                    <td>{{ $chapter->id }}</td>
                    <td>{{ $chapter->title }}</td>
                    <td>{{ $chapter->slug }}</td>
                    <td>{{ $chapter->description}}</td>
                    <td>{{ $chapter->difficulty}}</td>
                    <td>{{ $chapter->is_paid ? 'Paid' : 'Free'}}</td>
                    <td>{{ $chapter->estimated_time}} </td>
                    <td>{{ $chapter->created_at->format('d/m/y H:i') }}</td>
                    <td>{{ $chapter->updated_at->format('d/m/y H:i') }}</td>
                    <td><a href="{{ action('ChaptersController@chapter', [$chapter->slug])}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a></td>
                    <td><a href="{{ action('AdminController@editChapter',[$chapter->id])}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                    <td>
                        <form action="{{ action('AdminController@chapterDelete')}}" method="post" class="form-delete">
                            <input type="hidden" name="chapter" value="{{$chapter->id}}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-xs btn-danger delete">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif 
        </tbody>
    </table>
@endsection