@extends('layouts.admin')

@section('title', 'Edit Choice')

@section('content')
	<h1>Edit Choice</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@questionChoices', [$choice->question->id])}}"><span aria-hidden="true">&larr;</span> {{$choice->question->question}}</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::model($choice,['method' => 'POST', 'action' => ['AdminController@choiceUpdate', $choice->id], 'files' => true]) !!}
        @include('admin-partials._choice', ['submitText' => 'Update Choice'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection