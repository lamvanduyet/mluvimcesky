@extends('layouts.admin')

@section('title', 'Add Lesson')

@section('content')
	<h1>Add Lesson</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@lessons')}}"><span aria-hidden="true">&larr;</span> Lessons</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::open(['method' => 'POST','url' => 'admin/lessons/create', 'files' => true]) !!}
        @include('admin-partials._lesson', ['submitText' => 'Add Lesson'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection