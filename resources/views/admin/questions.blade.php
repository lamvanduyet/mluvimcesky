@extends('layouts.admin')

@section('title', 'Questions')

@section('content')
	<h1>Questions for test: <small>{{$test->name}}</small></h1>
	<hr>

    <div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@tests')}}"><span aria-hidden="true">&larr;</span> Tests</a>
            </li>
            <li>
                <a href="{{ action('AdminController@addQuestion', [$test->id])}}" class="btn btn-sm btn-info"><i class="fa fa-check-square-o"></i> Add Questions</a>
            </li>
        </ul>   
    </div>
   

	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Test No.</th>
                {{-- <th>Type</th> --}}
                <th>Question</th>
                <th>Choices</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Show Choices</th>
                <th>Add Choice</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Test No.</th>
                {{-- <th>Type</th> --}}
                <th>Question</th>
                <th>Choices</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Show Choices</th>
                <th>Add Choice</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($test->questions))
            @foreach($test->questions as $question)
                <tr>
                    <td>{{ $question->id }}</td>
                    <td>{{ $question->test_id }}</td>
                    {{-- <td>{{ $question->type }}</td> --}}
                    <td>{{ $question->question}}</td>
                    <td><a href="{{action('AdminController@questionChoices', [$question->id])}}">{{ count($question->choices) }}</a></td>
                    <td>{{ $question->created_at->format('d/m/y H:i') }}</td>
                    <td>{{ $question->updated_at->format('d/m/y H:i') }}</td>
                    <td><a href="{{action('AdminController@questionChoices', [$question->id])}}" class="btn btn-sm btn-warning">Show choices</a></td>
                    <td><a href="{{action('AdminController@addChoice', [$question->id])}}" class="btn btn-sm btn-info">Add choice</a></td>
                    <td><a href="{{ action('AdminController@editQuestion', [$question->id]) }}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                    <td>
                        <form action="{{ action('AdminController@questionDelete')}}" method="post" class="form-delete">
                            <input type="hidden" name="question" value="{{$question->id}}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs delete">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif    
        </tbody>
    </table>


@endsection