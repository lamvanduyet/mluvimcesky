@extends('layouts.admin')

@section('title', 'Users')

@section('content')
	<h1>Users</h1>
	<hr>

	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Role</th>
                <th>Username</th>
                <th>Email</th>
                <th>Created At</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Role</th>
                <th>Username</th>
                <th>Email</th>
                <th>Created At</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($users))
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->role }}</td>
                    <td>{{ $user->username}}</td>
                    <td>{{ $user->email}}</td>
                    <td>{{ $user->created_at->format('d/m/y H:i')}}</td>
                </tr>
            @endforeach
        @endif    
        </tbody>
    </table>


@endsection