
@extends('layouts.admin')

@section('title', 'Add Choice')

@section('content')
	<h1>Add Choice</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@questionChoices', [$question->id])}}"><span aria-hidden="true">&larr;</span> {{$question->question}}</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::open(['method' => 'POST','action' => ['AdminController@choiceCreate', $question->id], 'files' => true]) !!}
		@include('admin-partials._choice', ['submitText' => 'Add Choice'])
    {!! Form::close() !!}
    </div>
	</div>
@endsection