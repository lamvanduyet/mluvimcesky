@extends('layouts.admin')

@section('title', 'Tests')

@section('content')
	<h1>Tests</h1>
	<hr>

	<table class="table table-striped responsive table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Chapter</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Questions</th>
                <th>Created At</th>
                <th>Updated at</th>
                <th>View</th>
                <th>Add Question</th>
                <th>Show Question</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>No.</th>
                <th>Chapter</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Description</th>
                <th>Difficulty</th>
                <th>Is Paid</th>
                <th>Estimated Time/min</th>
                <th>Questions</th>
                <th>Created At</th>
                <th>Updated at</th>
                <th>View</th>
                <th>Add Question</th>
                <th>Show Question</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </tfoot>
        <tbody>
        @if(count($tests))
            @foreach($tests as $test)
                <tr>
                    <td>{{ $test->id }}</td>
                    <td>{{ $test->chapter->title }}</td>
                    <td>{{ $test->name }}</td>
                    <td>{{ $test->slug }}</td>
                    <td>{{ $test->description}}</td>
                    <td>{{ $test->difficulty}}</td>
                    <td>{{ $test->is_paid ? 'Paid' : 'Free'}}</td>
                    <td>{{ $test->estimated_time}} </td>
                    <td><a href="{{ action('AdminController@testQuestions', [$test->id])}}">{{ count($test->questions)}}</a></td>
                    <td>{{ $test->created_at->format('d/m/y H:i') }}</td>
                    <td>{{ $test->updated_at->format('d/m/y H:i') }}</td>
                    <td><a href="{{ action('TestsController@test', [$test->slug])}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a></td>
                    <td><a href="{{ action('AdminController@addQuestion', [$test->id])}}" class="btn btn-sm btn-info"><i class="fa fa-check-square-o"></i> Add Question</a></td>
                    <td><a href="{{ action('AdminController@testQuestions', [$test->id])}}" class="btn btn-sm btn-warning">Show Questions</a></td>
                    <td><a href="{{ action('AdminController@editTest',[$test->id])}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                    <td>
                        <form action="{{ action('AdminController@testDelete')}}" method="post" class="form-delete">
                            <input type="hidden" name="test" value="{{$test->id}}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs delete">Delete</button>
                        </form>

                    </td>
                </tr>
            @endforeach
        @endif 
        </tbody>
    </table>
@endsection