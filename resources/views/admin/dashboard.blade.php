@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')
	<h1>Dashboard</h1>
	<hr>
  <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title text-center">Lessons/Tests</h3>
      </div>
      <div class="panel-body">
        <div id="mychart" style="height: 250px; width: auto; "></div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>

  Morris.Donut({
  element: 'mychart',
  data: [
    {label: "Lessons", value: {{count($lessons)}} },
    {label: "Tests", value: {{count($tests)}} },
  ]
});
</script>
@endsection