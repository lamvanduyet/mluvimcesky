@extends('layouts.admin')

@section('title', 'Edit Question')

@section('content')
	<h1>Edit Question</h1>
	<hr>

	<div class="helper-buttons">
        <ul class="list-inline">
            <li>
                <a href="{{action('AdminController@testQuestions', [$question->test->id])}}"><span aria-hidden="true">&larr;</span> {{$question->test->name}}</a>
            </li>
        </ul>   
    </div>

	<div class="panel panel-default">
	<div class="panel-body">
	{!! Form::model($question,['method' => 'POST', 'action' => ['AdminController@questionUpdate', $question->id], 'files' => true]) !!}
        @include('admin-partials._question', ['submitText' => 'Update Question'])
    {!! Form::close() !!}
    </div>
	</div>
	
@endsection